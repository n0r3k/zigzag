<?php

/**
 * @see Zend_Image_Driver_Exception
 */
require_once 'Zend/Image/Driver/Exception.php';


/**
 * @see Zend_Image_Driver_Abstract
 */
require_once 'Zend/Image/Driver/Abstract.php';

/**
 * GdImage driver
 *
 * @category    Zend
 * @package     Zend_Image
 * @subpackage  Zend_Image_Driver
 * @author      Leonid A Shagabutdinov <leonid@shagabutdinov.com>
 * @author      Stanislav Seletskiy <s.seletskiy@office.ngs.ru>
 */
class Zend_Image_Driver_Gd extends Zend_Image_Driver_Abstract
{
    /**
     * Load image from $fileName
     *
     * @throws Zend_Image_Driver_Exception
     * @param string $fileName Path to image
     */
    public function load( $fileName )
    {
        parent::load( $fileName );
        
        $this->_imageLoaded = false;
        
        $info = getimagesize( $fileName );
        switch( $this->_type ) {
            case 'jpg':
                $this->_image = imageCreateFromJpeg( $fileName );
                if ( $this->_image !== false ) {
                    $this->_imageLoaded = true;
                }
                break;
            case 'png':
                $this->_image = imageCreateFromPng( $fileName );
                $this->_handleTransparentColor($this->_image);
                if ( $this->_image !== false ) {
                    $this->_imageLoaded = true;
                }
                break;
            case 'gif':
                $this->_image = imageCreateFromGif( $fileName );
                $this->_handleTransparentColor($this->_image);
                if ( $this->_image !== false ) {
                    $this->_imageLoaded = true;
                }
                break;
        }
    }


    /**
     * Get image size
     *
     * @throws Zend_Image_Driver_Exception
     * @return array Format: array( width, height )
     */
    public function getSize()
    {
        if( $this->_image === null ) {
            throw new Zend_Image_Driver_Exception(
                'Trying to get size of non-loaded image'
            );
        }

        return array(
            imagesx( $this->_image ),
            imagesy( $this->_image )
        );
    }


    /**
     * Get image contents
     *
     * @return string
     */
    public function getBinary()
    {
        if( $this->_image === null ) {
            throw new Zend_Image_Driver_Exception(
                'Trying to get size of non-loaded image'
            );
        }
        
        ob_start();
        imagejpeg( $this->_image );
        return ob_get_clean();
    }


    /**
     *
     * @throws Zend_Image_Driver_Exception
     * @param string $file File name to save image
     * @param string $type File type to save
     */
    public function save( $file, $type = 'auto' )
    {
         if ( !$this->_image ) {
             throw new Zend_Image_Driver_Exception(
                 'Trying to save non-loaded image'
             );
         }

         if ( $type == 'auto' ) {
             $type = $this->getType();
         }

         switch ( $type ) {
             case 'jpg': case 'jpeg':
                imagejpeg( $this->_image, $file, 100 );
                break;
            case 'png':
                imagepng( $this->_image, $file );
                break;
            case 'gif':
                imagegif( $this->_image, $file );
                break;
             default:
                 throw new Zend_Image_Driver_Exception(
                    'Undefined image type: "' . $type . '"'
                 );
                 break;
         }
        
    }


    /**
     * Resize image into specified width and height
     *
     * @throws Zend_Image_Driver_Exception
     * @param int $width Width to resize
     * @param int $height Height to resize
     * @return bool
     */
    public function resize( $width, $height )
    {
        parent::resize( $width, $height );
        
        $imageSize = $this->getSize();
        $resizedImage = imagecreatetruecolor( $width, $height );
        $successfull = imagecopyresampled(
            $resizedImage, $this->_image,
            0, 0,
            0, 0,
            $width, $height,
            $imageSize[ 0 ], $imageSize[ 1 ]
        );

        unset( $this->_image );
        $this->_image = $resizedImage;

        return $successfull;
    }

    
    /**
     * Crop image into specified frame
     *
     * @throws Zend_Image_Driver_Exception
     * @param int $left Left point to crop from
     * @param int $top Top point to crop from
     * @param int $width Width to crop
     * @param int $height Height to crop
     * @return bool
     */
    public function crop( $left, $top, $width, $height )
    {
        parent::crop( $left, $top, $width, $height );

        $imageSize = $this->getSize();
        $croppedImage = imagecreatetruecolor( $width, $height );
        $successfull = imagecopyresampled(
            $croppedImage, $this->_image,
            0, 0,
            $left, $top,
            $width, $height,
            $width, $height
            // $left + $width, $top + $height
        );

        unset( $this->_image );
        $this->_image = $croppedImage;

        return $successfull;

    }
    
    /**
     * If image is GIF or PNG keep transparent colors
     * 
     * @credit http://github.com/maxim/smart_resize_image/tree/master
     * @param $image src of the image
     * @return the modified image
     */
    protected function _handleTransparentColor($image = null) {
            $image = is_null ( $image ) ? $this->_image : $image;
            
            if (($this->_type == 'gif') || ($this->_type == 'png')) {
                $trnprt_indx = imagecolortransparent ( $this->_image );
                
                // If we have a specific transparent color
                if ($trnprt_indx >= 0) {
                        // Get the original image's transparent color's RGB values
                        $trnprt_color = imagecolorsforindex ( $this->_image, $trnprt_indx );
                        
                        // Allocate the same color in the new image resource
                        $trnprt_indx = imagecolorallocate ( $image, $trnprt_color ['red'], $trnprt_color ['green'], $trnprt_color ['blue'] );
                        
                        // Completely fill the background of the new image with allocated color.
                        imagefill ( $image, 0, 0, $trnprt_indx );
                        
                        // Set the background color for new image to transparent
                        imagecolortransparent ( $image, $trnprt_indx );
                } elseif ($this->_type == 'png') {
                        // Always make a transparent background color for PNGs that don't have one allocated already
                        // Turn off transparency blending (temporarily)
                        imagealphablending ( $image, false );
                        
                        // Create a new transparent color for image
                        $color = imagecolorallocatealpha ( $image, 0, 0, 0, 127 );
                        
                        // Completely fill the background of the new image with allocated color.
                        imagefill ( $image, 0, 0, $color );
                        
                        // Restore transparency blending
                        imagesavealpha ( $image, true );
                }
                return $image;
            }
    }

    /**
     * Link to gd-image resource
     *
     * @type resource
     */
    private $_image = null;
}