<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function _initControllerPlugins()
	{
		$plugin = Zend_Controller_Front::getInstance()->registerPlugin(
			new Ideo_ModuleConfig()
		);
	}

	protected function _initView()
	{
		// Session
		Zend_Session::start();
//		Zend_Session::rememberMe(60*60*24);
//		Zend_Session::forgetMe();

		// MySQL
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

		$db = Zend_Db::factory($config->database);

		Zend_Db_Table_Abstract::setDefaultAdapter($db);

		$db->query("SET NAMES UTF8");
		$db->getProfiler()->setEnabled(true);
		$registry = Zend_Registry::getInstance();
		$registry->config = $config;
		$registry->db     = $db;

		include_once 'Smarty/Smarty.class.php';                

		Zend_Layout::startMvc();

		// Resources
		$resourceLoader = new Zend_Loader_Autoloader_Resource(array(
			'basePath' => APPLICATION_PATH,
			'namespace' => '',
		));

		$resourceLoader->addResourceTypes(array(
			'model' => array(
				'path'      => 'models/',
				'namespace' => 'Model'
			),
			'table' => array(
				'path'      => 'tables/',
				'namespace' => 'Table'
			),
			'module' => array(
				'path'		=> 'modules/',
				'namespace' => 'Module'
			),
			'language' => array(
				'path'		=> 'languages/',
				'namespace' => 'Language'
			),
			'plugin' => array(
                'path'      => 'plugins/',
                'namespace' => 'Plugin'
            )
		));

		// No such var
		// return $view;
	}

	protected function _initRouter()
	{
		$router = Zend_Controller_Front::getInstance()->getRouter();
				
		/* Default (with action) route */

		$route = new Zend_Controller_Router_Route(
		  	':language/:controller/:action/:id/*',
		  	array(
				'language'		=> 'lv',
		  		'module'		=> 'default',
				'controller'	=> 'index',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('default_with_action', $route);
		/* DEFAULT MODULE */

		/* Pages */
		$route = new Zend_Controller_Router_Route(
			':language/:id/*',
			array(
				'module'		=> 'default',
				'controller'	=> 'page',
				'language'		=> 'lv',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('front_page', $route);
		
		$route = new Zend_Controller_Router_Route(
		  	':language/atsauksmes/',
		  	array(
				'language'		=> 'lv',
		  		'module'		=> 'default',
				'controller'	=> 'atsauksmes',
				'action'		=> 'index'
			)
		);
		$router->addRoute('default_atsauksmes', $route);
				
		$route = new Zend_Controller_Router_Route(
		  	':language/katalogs/:id',
		  	array(
				'language'		=> 'lv',
		  		'module'		=> 'default',
				'controller'	=> 'katalogs',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('default_katalogs', $route);				

		$route = new Zend_Controller_Router_Route(
		  	':language/grozs/:action',
		  	array(
				'language'		=> 'lv',
		  		'module'		=> 'default',
				'controller'	=> 'grozs',
				'action'		=> 'index'
			)
		);
		$router->addRoute('default_grozs', $route);				

		$route = new Zend_Controller_Router_Route(
		  	':language/darbnica/:action',
		  	array(
				'language'		=> 'lv',
		  		'module'		=> 'default',
				'controller'	=> 'darbnica',
				'action'		=> 'index'
			)
		);
		$router->addRoute('default_darbnica', $route);
		
		$route = new Zend_Controller_Router_Route(
			':language/',
			array(
				'module'		=> 'default',
				'controller'	=> 'index',
				'language'		=> 'lv',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('front_index', $route);

		/**********/
		/*  CART  */
		/**********/
		$route = new Zend_Controller_Router_Route(
			'status',
			array(
				'module'		=> 'default',
				'controller'	=> 'grozs',
				'action'		=> 'status',
				'id'			=> ''
			)
		);
		$router->addRoute('payment_status', $route);	

		$route = new Zend_Controller_Router_Route(
			'return',
			array(
				'module'		=> 'default',
				'controller'	=> 'grozs',
				'action'		=> 'return',
				'id'			=> ''
			)
		);
		$router->addRoute('payment_return', $route);
		
		/**********/
		/*  MISC  */
		/**********/
		
		/* AJAX route */
		$route = new Zend_Controller_Router_Route(
			'ajax/:action',
			array(
				'module'		=> 'default',
				'controller'	=> 'ajax',
				'action'		=> 'index'
			)
		);
		$router->addRoute('ajax', $route);
		
		/* Admin route */
		$route = new Zend_Controller_Router_Route(
			'admin/:controller/:action/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'index',
				'action'		=> 'index'
			)
		);
		$router->addRoute('admin', $route);

		/* Admin route */
		$route = new Zend_Controller_Router_Route(
			'admin/feedback/:action/:id/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'feedback',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('feedback', $route);


		$route = new Zend_Controller_Router_Route(
			'admin/gallery/:action/:id/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'gallery',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('gallery', $route);

		/* Orders */
		$route = new Zend_Controller_Router_Route(
			'admin/order/:action/:id/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'order',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('order', $route);
		
		/* Pages */
		$route = new Zend_Controller_Router_Route(
			'admin/page/:id/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'page',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('page', $route);		

		/* Pages */
		$route = new Zend_Controller_Router_Route(
			'admin/navigation/:id/*',
			array(
				'module'		=> 'admin',
				'controller'	=> 'navigation',
				'action'		=> 'index',
				'id'			=> ''
			)
		);
		$router->addRoute('navigation', $route);	

		return $router;
    }
}