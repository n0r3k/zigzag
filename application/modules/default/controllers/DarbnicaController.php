<?php

class DarbnicaController extends Ideo_Controller_Action
{
	protected $lang;
	
    public function indexAction()
    {
		$request = $this->getRequest();
		$this->lang = $request->getParam('language', 'lv');
	}
}