<?php

class IndexController extends Ideo_Controller_Action
{
	protected $lang;
	
    public function indexAction()
    {
		$request = $this->getRequest();
		$this->lang = $request->getParam('language', 'lv');
		$image = new Model_Image();
		
		$page = new Model_Page('tava-milaka-gultas-vela', $this->lang);
		$this->view->par_mums = $page->data;

		// gallery slider
		$gallery = new Model_Gallery();
		$gallery_list = $gallery->listActiveItems(false, $this->lang);

		if ($gallery_list)
		{
			foreach($gallery_list as &$v)
			{
				$image = new Model_Image();
				$v['first_image'] = $image->getFirst('gallery', $v['pgal_id']);
			}
		}
		
		$this->view->gallery_list = $gallery_list;		
	}
}