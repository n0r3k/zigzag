<?php

class GrozsController extends Ideo_Controller_Action
{
	const PAYMENT_STATUS_SUCCESS = 1;
	const PAYMENT_STATUS_CANCELED = 2;

	const MERCHANT_ID = 241;
	const MERCHANT_SECRET = 'blablaSecret123key@';

	protected $lang;
	
	public function init()
	{
		parent::init();

		$request = $this->getRequest();
		$this->lang = parent::getLanguage();
	}
	
	public function indexAction()
	{
		$cart = new Model_Cart();
		
		$this->view->cart_items = $cart->getItems();
		$this->view->prices = $cart->getPrices();
	}
	
	public function pasutijumsAction()
	{
		$cart = new Model_Cart();
		$prices = $cart->getPrices();
		$this->view->prices = $prices;	

		// get request	
		$request = $this->getRequest();

		// country classifier
		$countries = new Model_Countries();
		$country_list = $countries->listActive($this->lang);
		$this->view->country_list = $country_list;

		// fetch client info from session if exists
		$cart = new Zend_Session_Namespace('Cart');
		$cl_info = &$cart->cl_info;		
		$cart_items = $cart->items;
		
		// if form submitted 
		if ($request->isPost())
		{	
			// billing
			$cl_info['order_id'] 			= $order_data['pord_id'] 			= $request->getParam('order_id');
			$cl_info['billingFirstName'] 	= $order_data['pord_cl_first_name'] = $request->getParam('billingFirstName');		
			$cl_info['billingLastName'] 	= $order_data['pord_cl_last_name'] 	= $request->getParam('billingLastName');		
			$cl_info['billingEmail'] 		= $order_data['pord_cl_email'] 		= $request->getParam('billingEmail');		
			$cl_info['newsletterOptIn'] 	= $order_data['pord_cl_newsletter']	= $request->getParam('newsletterOptIn') ? 1 : 0; // todo : newsletter table, unsubscribe support
			$cl_info['billingPhone'] 		= $order_data['pord_cl_billing_phone'] = $request->getParam('billingPhone');
			$cl_info['billingAdditionalInfo'] = $order_data['pord_cl_additional_info'] = $request->getParam('billingAdditionalInfo');
			// delivery

			$cl_info['deliveryNameMatchesBilling'] = $request->getParam('deliveryNameMatchesBilling') ? 1 : 0;

			if ($cl_info['deliveryNameMatchesBilling'] == 1)
			{
				$cl_info['deliveryName'] 	= $order_data['pord_cl_delivery_name'] 	= $request->getParam('billingFirstName');
				$cl_info['deliveryPhone'] 	= $order_data['pord_cl_delivery_phone'] = $request->getParam('billingPhone');
			}
			else
			{
				$cl_info['deliveryName'] 	= $order_data['pord_cl_delivery_name'] 	= $request->getParam('deliveryName');
				$cl_info['deliveryPhone'] 	= $order_data['pord_cl_delivery_phone'] = $request->getParam('deliveryPhone');			
			}

			$cl_info['deliveryCountry'] = $order_data['pord_cl_delivery_country'] 	= $request->getParam('deliveryCountry');
			$cl_info['deliveryCity'] 	= $order_data['pord_cl_delivery_city'] 		= $request->getParam('deliveryCity');
			$cl_info['deliveryStreet'] 	= $order_data['pord_cl_delivery_street'] 	= $request->getParam('deliveryStreet');
			$cl_info['deliveryZip'] 	= $order_data['pord_cl_delivery_zip'] 		= $request->getParam('deliveryZip');

			$countries = new Model_Countries($cl_info['deliveryCountry']);
			$cl_info['deliveryCountryCode'] = $countries->data['clsc_code'];

			// validation
			if ($request->getParam('confirmData'))
			{
				// create order
				$order = new Model_Order($order_data['pord_id']);
				$order->data = $order_data;
				$order->data['pord_date'] = date( 'Y-m-d' );
				$order->data['pord_cost_id'] = Model_OrderStatus::getIdByCode( Model_OrderStatus::ORDER_STATUS_CODE_NEW );

				// set prices
				$order->data['pord_invoice_subtotal'] 	= $prices['_subtotal'];
				$order->data['pord_invoice_vat'] 		= $prices['_vat'];
				$order->data['pord_invoice_total']		= $prices['_total'];
				$order->data['pord_clsl_code'] 			= $this->lang;

				try {
					if (empty($cl_info['order_id'])) {
						$cl_info['order_id'] = $order->save();
					} else {
						$order->save();
					}
				} catch (Exception $e) {
					throw new Exception( $e->getMessage() );
				}

				// save order items
				$order_items = new Model_OrderItems($cl_info['order_id']);
				// delete all items
				$order_items->deleteItems();
				// create items
				if ( $cart_items )
				{
					foreach ( $cart_items as $item )
					{
						$order_items->data['poit_pord_id'] 	= $cl_info['order_id'];
						$order_items->data['poit_pgal_id'] 	= isset($item['_item']['pgal_id']) ? $item['_item']['pgal_id'] : null;
						$order_items->data['poit_pfac_id'] 	= isset($item['_item']['pfac_id']) ? $item['_item']['pfac_id'] : null;
						$order_items->data['poit_clss_id1'] = $item['_size1']['clss_id'];
						$order_items->data['poit_clss_id2'] = $item['_size2']['clss_id'];
						$order_items->data['poit_clss_id3'] = $item['_size3']['clss_id'];
						$order_items->data['poit_clss_id4'] = isset($item['_size4']) ? $item['_size4']['clss_id'] : null;
						$order_items->data['poit_price'] 	= $item['_total'];
						$order_items->data['poit_qty'] 		= $item['_qty'];
						$order_items->data['poit_total']	= $item['_item_total'];
						
						$order_items->save();
					}
				}
				
				$factory = new Model_Factory;
				$factory->clear();
				
				// redirect
				$this->_redirect('/' . $this->lang . '/grozs/apmaksa');
			}
		}
		elseif (!$cl_info)
		{
			// set default values
			$cl_info['deliveryNameMatchesBilling'] = 1;
			$cl_info['deliveryCountry'] = 126;	
		}

		$this->view->cl_info = $cl_info;
	}
	
	public function apmaksaAction()
	{
		// fetch client info from session if exists
		$cart = new Zend_Session_Namespace('Cart');
		$cl_info = &$cart->cl_info;		
		$prices = $cart->prices;
		
		// init airpay
		$airpay = new External_AirPay( self::MERCHANT_ID, self::MERCHANT_SECRET );

		// init cache
		$cache = Zend_Registry::get('Cache');

		if (!$psystems = $cache->load( 'airpay_psystems' )) 
		{
		    $psystems = $airpay->psystems();
		    $cache->save( $psystems, 'airpay_psystems' );
		}

		$lang_decode = array('lv' => 'LAT', 'en' => 'ENG', 'ru' => 'RUS', 'no' => 'ENG');

		$invoice = array(
			'amount'	=> $prices['_total'] * 100,			// minor units, e.g. 1 for 0.01
			'currency'	=> 'EUR',							// currency code in ISO 4217
			'invoice'	=> $cl_info['order_id'],			// unique transaction value
			'language'	=> $lang_decode[$this->lang],		// language: LAT, RUS, ENG
			'cl_fname'	=> $cl_info['billingFirstName'],	// client's first name
			'cl_lname'	=> $cl_info['billingLastName'],		// client's last name
			'cl_email'	=> $cl_info['billingEmail'],		// client's e-mail address
			'cl_country'=> $cl_info['deliveryCountryCode'],	// country code in ISO 3166-1-alpha-2
			'cl_city'	=> $cl_info['deliveryCity'],		// city name
			'description'	=> 'Pasūtījums Nr. ' . $cl_info['order_id'],			// description of the transaction, visible to the client, e.g. description of the product
			'psys'		=> ''								// payment system alias. empty for default or taken from $airpay->psystems
		);

		$this->view->airpay_url	 = $airpay->url;
		$this->view->airpay_form = $airpay->payment($invoice);
	}

	public function kopsavilkumsAction()
	{
		$cart = new Zend_Session_Namespace('Cart');
		$cl_info = $cart->cl_info;		

		if ( isset( $cl_info['order_id'] ) )
		{
			$order = new Model_Order( $cl_info['order_id'] );
			$order_items = new Model_OrderItems( $cl_info['order_id'] ); 	

			$katalogs_orders = $order_items->listItems();
			$darbnica_orders = $order_items->listFactoryItems();

			$this->view->order_items = array_merge($katalogs_orders, $darbnica_orders);
			$this->view->order = $order->data;

			// clear all cart data
			$cart = new Model_Cart();		
			$cart->clear();
		} 
	}
	
	public function statusAction()
	{
		// Disables skeleton layout
	    $this->_helper->layout()->disableLayout();
	    // Disables action view rendering
	    $this->_helper->viewRenderer->setNoRender(true);
		
		// request
		$request = $this->getRequest();

		// init airpay
		$airpay = new External_AirPay( self::MERCHANT_ID, self::MERCHANT_SECRET );

		// init logs
		$logs = new Model_AirpayStatusLogs();
		$log_entry = array();
		$log_entry['slog_params'] = serialize( $request->getParams() );
				
		if ( !$ret = $airpay->response( $request->getParams(), 'status' ) )
		{
			// log post
			$log_entry['slog_success'] = 0;
		}
		else
		{
			$log_entry['slog_success'] = 1;	
		}
		
		if ( $request->getParam('manual') == 'override' )
		{			
			$ret = $request->getParams();			
		}
		
		$status = $ret['status_id'];			// maksājuma statuss
		$tr_id 	= $ret['mc_transaction_id'];	// transakcijas id - izveidotais pasūtījuma id 

		// handle payment
		$order = new Model_Order($tr_id);

		if (!$order->data)
		{
			return false;	
		}
		
		$this->lang = $order->data['pord_clsl_code'];

		// set lang from order
		$translate = Zend_Registry::get('Zend_Translate');
		$translate->setLocale( !empty($this->lang) ? $this->lang : 'lv' );
		$translate->addTranslation(APPLICATION_PATH . '/modules/default/languages/' .  $this->lang . '.csv', $this->lang);
		$this->view->tr = $translate;

		$logs->save($log_entry);
		
		// save data
		$order->data['pord_airpay_code'] = $status;
		$order->save();
				
		// generate invoice (only if payment successful)
		if ($status == self::PAYMENT_STATUS_SUCCESS)
		{
			// reload order data
			$order = new Model_Order($tr_id);
			// generate invoice number 
			$order->data['pord_invoice_number'] = $order->generateInvoiceNumber();
					
			require_once(ROOT_PATH . '/library/Ideo/Dompdf/dompdf_config.inc.php');		
			spl_autoload_register('DOMPDF_autoload'); 		
			
			$order->data['clsc_name'] = $order->data['clsc_name_' . $order->data['pord_clsl_code']];

			$order->data['pord_invoice_total_lvl'] = number_format($order->data['pord_invoice_total'] * Model_Cart::EUR_TO_LVL, 2);
			$order->data['pord_invoice_total_words'] = Ideo_NumberConversion::numberToWords($order->data['pord_invoice_total'], $this->lang, 'EUR');
		
			$this->view->order = $order->data;
			$this->view->is_invoice = 1;

			// Order items
			$order_items = new Model_OrderItems($tr_id);

			$katalogs_orders = $order_items->listItems($this->lang);
			$darbnica_orders = $order_items->listFactoryItems();
			
			$this->view->order_items = array_merge($katalogs_orders, $darbnica_orders);
		
			$html = $this->view->render( APPLICATION_PATH . '/modules/admin/views/scripts/order/invoice.phtml' );		

			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->render();
			$invoice_content = $dompdf->output();						
					
			// send e-mail to customer and zig-zag		
			switch ($this->lang)
			{
				case 'lv':	
					$subject = 'Pasūtījums Nr.: ' . $tr_id;
					$body = 'Sveiki!<br><br>
							Paldies par Jūsu pasūtījumu, esam to saņēmuši. Jūsu ideālā gultas veļa dos Jums daudz prieka un labas sajūtas.<br><br>
							Ja būs nepieciešams, ZIGZAGFACTORY klientu konsultants sazināsies ar Jums, lai precizētu pasūtījumu un Jūs saņemtu tieši to, ko vēlējāties.<br><br><br>
							Ar sveicieniem Jūsu ideālā gultas veļa ZIGZAG FACTORY.<br><br>
							<span style="font-size:9px;">*Šis ir automātiski izveidots apstiprinājums, nav nepieciešams uz to atbildēt.</span>'; break;
				case 'ru':
					$subject = 'Заказ Но.: ' . $tr_id;
					$body = 'Добрый день!<br><br>
							Спасибо за Ваш заказ, мы его получили. Ваше идеальное постельное бельё подарит Вам много радости прчтных ощущений.<br><br>
							Если будет необходимость, консультант ZIGZAG FACTORY свяжется с Вами для уточнения заказа, чтобы Вы получили именно то, что желаете.<br><br><br>
							С наилучшими пожеланиями Ваше идеальное постельное бельё ZIGZAG FACTORY.<br><br>
							<span style="font-size:9px;">*Это автоматическое подтверждение, отвечать на него не надо.</span>'; break;
				default:
					$subject = 'Order No.: ' . $tr_id;
					$body = 'Hello!<br><br>
							Thank you for your order, we have received it. Your ideal bed linen will give you lots of joy and good feelings.<br><br>
							If necessary, a ZIGZAG FACTORY customer service representative will contact you to verify if the order is correct so that you get exactly what you wanted.<br><br><br>
							Best wishes, your ideal bed linen ZIGZAG FACTORY.<br><br>
							<span style="font-size:9px;">*This is an automatically generated confirmation, no need to answer.</span>'; break;
			}
		
			$mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($body);
			$mail->setFrom('order@zigzagfactory.com', 'ZIGZAG FACTORY');
			$mail->setReplyTo('info@zigzagfactory.com');
			$mail->addTo($order->data['pord_cl_email'], $order->data['pord_cl_first_name'] . ' ' . $order->data['pord_cl_last_name']);
			$mail->setSubject($subject);
			
			// attachment
			if ($invoice_content)
			{
				$attachment = new Zend_Mime_Part($invoice_content);
				$attachment->type = 'application/pdf';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = 'invoice-' . $order->data['pord_invoice_number'] . '.pdf'; // name of file
				
				$mail->addAttachment($attachment);			
			}
			
			// $mail->addBcc('info@zigzagfactory.com', 'ZIGZAG FACTORY');
			$mail->addBcc('ingars.ruikis@rixtech.lv', 'Admin');
			$mail->send();			
		}
	}
	
	public function returnAction()
	{
		// Disables skeleton layout
	    $this->_helper->layout()->disableLayout();
	    // Disables action view rendering
	    $this->_helper->viewRenderer->setNoRender(true);
	
		$request = $this->getRequest();
		$status = $request->getParam('status');

		// return?transaction_id=HA2014/05/03-1399079687&transaction_hash=2982ba3cc2b545727387866c2338eb3e&status=2

		// possible statuses:
		// 1 = SUCCESS
		// 2 = CANCELLED

		if ($status == self::PAYMENT_STATUS_SUCCESS)
		{
			$this->_redirect('/' . $this->lang . '/grozs/kopsavilkums');
		}
		else
		{
			$this->_redirect('/' . $this->lang . '/grozs/pasutijums');			
		}
	}
}
