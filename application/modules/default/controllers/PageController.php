<?php

class PageController extends Ideo_Controller_Action {
	public function indexAction()
	{
		$request = $this->getRequest();
		$code = $request->getParam('id');
		$lang = $request->getParam('language', 'lv');

		if ( in_array( $code, array( 'par-zig-zag', 'audumi' ) ) )
		{
			$page = new Model_Page(false, $lang);
			$this->view->pages = $page->getListByGroup($code);
		}
		elseif ( $code == 'gultas-vela' )
		{
			// custom page
			$this->view->gultas_vela = true;

			// feedback
			$feedback = new Model_Feedback();
			$feedback_list = $feedback->listItems($lang);
			
			if ($feedback_list)
			{
				foreach($feedback_list as &$v)
				{
					// strip text
					$v['lfbk_text'] = strlen($v['lfbk_text']) > 130 ? substr(strip_tags($v['lfbk_text']), 0, 130) . '...' : $v['lfbk_text'];

					$image = new Model_Image();
					$v['first_image'] = $image->getFirst('feedback', $v['pfbk_id']);
				}
			}
			
			$this->view->feedback_list = $feedback_list;		
		}
		else
		{
			$page = new Model_Page($code, $lang);
			$this->view->page = $page->data;
		}		
	}
}