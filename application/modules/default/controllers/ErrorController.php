<?php

class ErrorController extends Ideo_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
        
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->error_code = $this->getResponse()->getHttpResponseCode();
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Wooops, crash!';
                break;
        }
        
        $this->view->env = APPLICATION_ENV;
        $this->view->exception = $errors->exception;
        $this->view->request   = $errors->request;
    }


}

