<?php

class AtsauksmesController extends Ideo_Controller_Action
{
	protected $lang;
	
	public function init()
	{
		parent::init();

		$request = $this->getRequest();
		$this->lang = $request->getParam('language', 'lv');

	}
	
	public function indexAction()
	{
		$feedback = new Model_Feedback();
		$feedback_list = $feedback->listItems($this->lang);
		
		if ($feedback_list)
		{
			foreach($feedback_list as &$v)
			{
				$image = new Model_Image();
				$v['first_image'] = $image->getFirst('feedback', $v['pfbk_id']);
			}
		}
		
		$this->view->feedback_list = $feedback_list;		
	}
}
