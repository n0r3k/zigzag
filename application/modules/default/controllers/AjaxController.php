<?php

class AjaxController extends Ideo_Controller_Action {

	public function preDispatch()
	{
		// Disables skeleton layout
	    $this->_helper->layout()->disableLayout();
	    // Disables action view rendering
	    $this->_helper->viewRenderer->setNoRender(true);
	}


	/**
	 *	AJAX methods
	 *	[PUBLIC]
	 */
	public function loadgalleryitemsAction()
	{
		$request = $this->getRequest();
		$lang = $request->getParam('language', 'lv');
		// kolekcijas id
		$id = $request->getParam('id');

		$gallery = new Model_Gallery();
		$gal_list = $gallery->listActiveItems($id, $lang);

		$tr = Zend_Registry::get('Zend_Translate');
		$title_prefix = $tr->_('Gultasveļas<br>komplekts');

		if ($gal_list)
		{
			foreach ($gal_list as &$item)
			{
				$image = new Model_Image();
				$item['first_image'] = $image->getFirst('gallery', $item['pgal_id']);

				// Gallery title prefix
				$item['_title_prefix'] = $title_prefix;
			}
		}

		echo json_encode($gal_list);
	}


	public function getgallerydetailsbyidAction()
	{
		$request = $this->getRequest();
		$lang = $request->getParam('language', 'lv');
		$id = $request->getParam('id');

		// Gallery data
		$gallery = new Model_Gallery($id);

		// Gallery language data
		$gallery_lang = new Model_GalleryLanguage($id, $lang);

		// Images
		$image = new Model_Image();
		$image_list = $image->listItems('gallery', $id);

		if ($image_list)
		{
			foreach ($image_list as $i => $image)
			{
				// return only 4 images
				if ($i > 3) {
					break;
				}

				$response['images'][] = array('img_name' => $image['pimg_name']);
			}
		}

		// Translation
		$tr = Zend_Registry::get('Zend_Translate');

		// Response
		$response['id'] = $id;
		$response['name'] = $gallery_lang->data['lgal_title'];
		$response['description'] = nl2br($gallery_lang->data['lgal_description']);

		switch($lang) {
			case 'lv':
				$response['description'] .= '
					<br>
                	<b>Gultas veļa ir izgatavota no 100% tencela satīna auduma</b> – tas ir maigs, mīksts, viegls un izturīgs materiāls, kas regulē ķermeņa mitruma svārstības, uzlabos ādu un Jūsu veselību.
                	Un tam piemīt papildus visas eikalipta priekšrocības.
                	<br><br>
                	<b>Eikalipta auduma gultas veļa</b>
                	<ul style="padding-left:15px;margin-top:0px;">
                		<li>Maigs, mīksts, viegls un izturīgs materiāls.</li>
                		<li>Ar termoregulējošām īpašībām – radīs vēsuma sajūtu vasarā, bet ziemā sildīs.</li>
                		<li>Nelips Jums pie ādas, nesviedrēs un neelektrizēsies.</li>
                		<li>Piemērots jutīgai ādai, neizraisa kairinājumu.</li>
                		<li>Regulē ķermeņa mitruma svārstības, uzlabos ādu un Jūsu veselību.</li>
                		<li>Pilnīgi higiēnisks – kavēs baktēriju veidošanos.</li>
                	</ul>
                	<b>Kopšana</b><br>
                	Mazgāt veļas mašīnā 30°- 40°C temperatūrā, saudzīgā režīmā, izgriežot uz kreiso pusi, un tā saglabājot krāsu spilgtumu. Žāvēt dabiski. Ieteicams gludināt vēl nedaudz mitru. Neizmantot balinātāju!'; break;
			case 'en':
				$response['description'] .= '
					<br>
					<b>The bed linen is produced of 100% tencel satin fabric</b> - it is delicate, soft, light and durable, as well as it compensates from the fluctuations of body moisture, improves skin and overall health.
					<br><br>
					<b>Bed linen from eucalyptus fabric</b>
					<ul style="padding-left:15px;margin-top:0px;">
						<li>Delicate, soft, light and durable material.</li>
						<li>With thermo regulating properties - shall cool you during summer and keep you warm during winter.</li>
						<li>Shall not stick to the skin, make you sweat or electrify.</li>
						<li>Suitable for sensitive skin, does not cause irritation.</li>
						<li>Regulated body moisture fluctuations, improves skin and overall health.</li>
						<li>Fully hygienic - shall hinder the formation of bacteria.</li>
					</ul>
					<b>Treatment</b><br>
					To be machine washed in 30-40°C, in a delicate mode turned inside out so the colour retains its brightness.
					Ensure natural drying. Advisable iron while still a little wet.
					May be ironed in a delicate mode. Do not bleach.<br>'; break;
			case 'ru':
				$response['description'] .= '
					<br>
					<b>Постельно белье изготовлено из 100% сатинового тенселя</b> – это нежный, мягкий, легкий и прочный материал, который регулирует колебания влажности тела, улучшит Вашу кожу и здоровье.
					<br><br>
					<b>Постельное белье из эвкалипта</b>
					<ul style="padding-left:15px;margin-top:0px;">
						<li>Нежный, мягкий, легкий и прочный материал.</li>
						<li>Обладает терморегулирующими особенностями – летом создаст ощущение прохлады, а зимой согреет.</li>
						<li>Не липнет к Вашей коже, не вызывает потение и не электризуется.</li>
						<li>Подходит для чувствительной кожи, не вызывает раздражений.</li>
						<li>Регулирует колебания влажности тела, улучшает кожу и Ваше здоровье.</li>
						<li>Полностью гигиеничен – задерживает образование бактерий.</li>
					</ul>
					<b>Уход</b><br>
					Стирать в стиральной машине при температуре 30-40 градусов, в щадящем режиме, вывернув наизнанку – так Вы сохраните яркость красок.
					Сушить естественным образом. Желательно гладить немного влажным. Гладить только в бережном режиме.Не использовать отбеливатель.<br>'; break;
		}

		$response['items'] = array();

		$product_types = new Model_ProductTypes();
		$product_type_list = $product_types->listItems();

		if ($product_type_list)
		{
			foreach ($product_type_list as $type)
			{
				$model_sizes = new Model_SizePrices();
				$tmp_sizes = $model_sizes->listItemsByGalleryId($id, $type['cprt_code'], true);

				if ($tmp_sizes)
				{
					foreach ($tmp_sizes as $v)
					{
						$sizes[$type['cprt_code']][] =  array(
							'id' => $v['clss_id'],
							'size' => $v['clss_width'] . ' x ' . $v['clss_height'] . ' ' . $tr->_('cm'),
							'price' => $v['xgls_price'],
							'code' => $type['cprt_code']
						);
					}
				}
				else
				{
					$sizes[$type['cprt_code']][] =  array(
						'size' => $tr->_('nav pieejams'),
						'price' => 0.00,
						'code' => $type['cprt_code']
					);
				}

				$response['items'][] = array(
					'type' => $tr->_($type['cprt_code']),
					'sizes' => $sizes[$type['cprt_code']]
				);
			}
		}

		$response['totalPrice'] = 0;

		echo json_encode($response);
	}
	/**************
	 *	  CART    *
	 **************/
	public function addgalleryitemtocartAction()
	{
		$request = $this->getRequest();
		$id = $request->getParam('id');
		$size1 = $request->getParam('size1');
		$size2 = $request->getParam('size2');
		$size3 = $request->getParam('size3');
		$lang = parent::getLanguage();

		// Gallery data
		$gallery = new Model_Gallery($id, $lang);

		if (!$gallery->getId())
		{
			throw new Exception('gallery not found');
		}

		$cart = new Model_Cart();
		$cart->addGalleryItem($gallery, new Model_Size($size1), new Model_Size($size2), new Model_Size($size3));

		// return cart qty
		$prices = $cart->getPrices();

		$response['_qty'] = $prices['_qty'];

		echo json_encode($response);
	}

	public function addfactoryitemtocartAction()
	{
		$request = $this->getRequest();
		$lang = parent::getLanguage();

		$factory = new Model_Factory();

		// get data from session
		$items = $factory->getItems();

//		print_r($items);

		// save data to db
		if (!empty($items))
		{
			$pfac_id = $factory->save();

			$factory = new Model_Factory($pfac_id);

			foreach ($items as $type => $item)
			{
				if (in_array($type, array('spilvendrana-1', 'spilvendrana-2')))
				{
					$type = 'spilvendrana';	
				}
				
				$factory_items = new Model_FactoryItems();
				$product_type = new Model_ProductTypes();

				$factory_items->data['pfai_pfac_id'] = $pfac_id;
				$factory_items->data['pfai_clss_id'] = $item['size']['id'];
				$factory_items->data['pfai_cprt_id'] = $product_type->getIdByCode($type);
				$factory_items->data['pfai_content'] = serialize($item['layout']);
				$factory_items->data['pfai_bottom_color'] = $item['bottom_color'];
				$factory_items->data['pfai_option_aa'] = $item['aaId'];
				$factory_items->data['pfai_price'] = number_format($item['size']['base_price'] + $item['design_price'], 2);
			
				$factory->data['pfac_total'] += $factory_items->data['pfai_price'];

				try {
					$factory_items->save();
				} catch (Exception $e) {
					echo $e->getMessage();
					// print_r($factory_data);
				}
			}
			
			$factory->save();
		}

		// add item to cart
		$cart = new Model_Cart();
//		$cart->addGalleryItem($gallery, new Model_Size($size1), new Model_Size($size2), new Model_Size($size3));
		$cart->addFactoryItem($factory);

		// return cart qty
		$prices = $cart->getPrices();

		$response['_qty'] = $prices['_qty'];

		echo json_encode($response);
	}

	public function delgalleryitemfromcartAction()
	{
		$request = $this->getRequest();
		$cart_id = $request->getParam('id');

		$cart = new Model_Cart();
		$response = $cart->delGalleryItem($cart_id);

		echo json_encode($response);
	}

	public function setitemamountAction()
	{
		$request = $this->getRequest();
		$cart_id = $request->getParam('id');
		$amount = $request->getParam('amount');

		$cart = new Model_Cart();
		$response = $cart->setItemAmount($cart_id, $amount);

		echo json_encode($response);
	}

	public function clearcartAction()
	{
		$cart = new Model_Cart();
		$cart->clear();
	}

	/***************
	 *	 FACTORY   *
	 ***************/
	public function openfactorybytypeAction()
	{
		$request = $this->getRequest();
		$lang = $request->getParam('language', 'lv');
		$type = $request->getParam('type');

		// Translation
		$tr = Zend_Registry::get('Zend_Translate');

		// Response
		$response = array();

        $spilvendranaOptions = array(
            array('id' => 11, 'name' =>$tr->_('Ar aizloci uz iekšpusi, sānā')),
            array('id' => 12, 'name' =>$tr->_('Caurspīdīgas spiedpogas, sānā'))
        );

		// Options
		$options = array(
			'virspalags' => array(
				array('id' => 1, 'name' => $tr->_('Praktisks atvērums kājgalī')),
				array('id' => 2, 'name' => $tr->_('Caurspīdīgas spiedpogas kājgalī')),
				array('id' => 3, 'name' => $tr->_('Ar aizloci uz iekšpusi, kājgalī'))
			),
 			'palags' => array(
				array('id' => 4, 'name' =>$tr->_('Apstrādātas malas')),
				array('id' => 5, 'name' =>$tr->_('Gumija tikai stūros')),
				array('id' => 6, 'name' =>$tr->_('Gumija pa perimetru, matrača') . ' h-10 cm'),
				array('id' => 7, 'name' =>$tr->_('Gumija pa perimetru, matrača') . ' h-20 cm'),
				array('id' => 8, 'name' =>$tr->_('Gumija pa perimetru, matrača') . ' h-30 cm'),
				array('id' => 9, 'name' =>$tr->_('Gumija pa perimetru, matrača') . ' h-50 cm'),
				array('id' => 10, 'name' =>$tr->_('Gumija pa perimetru, matrača') . ' h-70 cm')
			),
			'spilvendrana-1' => $spilvendranaOptions,
            'spilvendrana-2' => $spilvendranaOptions
		);

		// Size
		$size = new Model_Size();
        if (strpos($type, 'spilvendrana') !== false) {
            $response['sizes'] = $size->listItems('spilvendrana');
        } else {
            $response['sizes'] = $size->listItems($type);
        }


		$response['colors'] = array( 'f4f3f8', 'f3ead8', 'ead6b5', 'f9ab84', 'b86547', 'e8b6bf',
									 'f51b69', 'e53831', 'fbdd2d', 'ff9041', 'a2d974', '0d7a36',
									 '814c76', 'a9d8ea', '43325c', '94817b', '232122' );

		$response['options'] = $options[$type];
 
		$response['confirm_message'] = $tr->_('Uzmanību! Mainot izmērus, Jūsu veidotais dizains netiks saglabāts.');
 
		echo json_encode($response);
	}

	public function savelayouttosessionAction()
	{
		$request = $this->getRequest();
		$type = $request->getParam('type');
		$size = $request->getParam('size');
		$layout = $request->getParam('layout');
		$bottom_color = $request->getParam('bottom_color');
		$design_price = $request->getParam('design_price');
		$option_aa = $request->getParam('aaId');

		$factory = new Model_Factory();
		$factory->saveLayout($type, $layout, $size, $bottom_color, $design_price, $option_aa);
	}

	public function loadlayoutfromsessionAction()
	{
		$request = $this->getRequest();
		$type = $request->getParam('type');

		$factory = new Model_Factory();
		$response = $factory->loadLayout($type);

		echo json_encode($response);
	}

    public function loadpreviewsfromsessionAction()
    {
        $request = $this->getRequest();
        $types = $request->getParam('types');
        $factory = new Model_Factory();
        $response = array();

        foreach ($types as $type)
        {
            $response[$type] = $factory->loadLayout($type);
        }

        echo json_encode($response);
    }
}
