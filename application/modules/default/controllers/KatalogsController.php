<?php

class KatalogsController extends Ideo_Controller_Action
{
	protected $lang;
	
	public function init()
	{
		parent::init();

		$request = $this->getRequest();
		$this->lang = $request->getParam('language', 'lv');

	}
	
	public function indexAction()
	{
		$request = $this->getRequest();
		$id = $request->getParam('id');
		$this->view->open_id = $id;		
	
		// Gallery categories
		$gallery_categories = new Model_GalleryCategories();
		$cat_list = $gallery_categories->listItems();
		$this->view->gallery_categories = $cat_list;

		// Galleries
		if ($cat_list)
		{
			$gallery = new Model_Gallery();
			$gallery_list = $gallery->listActiveItems(false, $this->lang);
	
			if ($gallery_list)
			{
				foreach($gallery_list as &$v)
				{
					$image = new Model_Image();
					$v['first_image'] = $image->getFirst('gallery', $v['pgal_id']);
				}
			}
			
			$this->view->gallery_list = $gallery_list;		
		}
	}
}
