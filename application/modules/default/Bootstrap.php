<?php

class Default_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initControllerPlugins()
	{
		$plugin = Zend_Controller_Front::getInstance()->registerPlugin(
			 new Plugin_LanguageSwitcher()
		);
	}

	public function defaultInitFunction()
	{
		// Smarty
		$path = APPLICATION_PATH . DS . 'modules' . DS . 'default' . DS . 'views' . DS . 'scripts';

		$view = new Zend_View_Smarty($path);

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
		$viewRenderer->setViewBasePathSpec($view->_smarty->template_dir)
            ->setViewScriptPathSpec(':controller/:action.:suffix')
            ->setViewScriptPathNoControllerSpec(':action.:suffix')
            ->setViewSuffix('phtml');

		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        // Set alternate link tags (for active languages)
        $paths = explode('/', $_SERVER['REQUEST_URI']);
        $rURI = count($paths) < 3 || empty($paths[2])
        		? ''
        		: substr($_SERVER['REQUEST_URI'], 2 + strlen($paths[1]));
        $view->_alternate = array('server' => 'http://'.$_SERVER['SERVER_NAME'], 'path' => $rURI);

		$this->bootstrap('frontController');
		$request = $this->getResource('frontController')->getRequest();
		$controller = $request->getControllerName();

		switch($controller)
		{
			case 'page':
				$param_id = $request->getParam('id');
				break;
			default:
				$param_id = $controller;
		}

        // SEO
        $nav_meta = new Model_Navigation($param_id, $request->getParam('language', 'lv'));

		// Layout
		$viewLayout = new Zend_View();
		$viewLayout->doctype('HTML5');
		$viewLayout->headTitle(!empty($nav_meta->data) ? $nav_meta->data['lnav_title'] : 'Zig Zag Factory');
		$viewLayout->headMeta()->setCharset('UTF-8')
						->setHttpEquiv('X-UA-Compatible', 'IE=edge,chrome=1')
						->setName('viewport', 'width=device-width, initial-scale=1, user-scalable=no')
						->setName('description', $nav_meta->data['lnav_meta_description'])
						->setName('keywords', $nav_meta->data['lnav_meta_keywords']);

		$viewLayout->headScript()
					    ->appendFile('/bower_components/jquery-legacy/dist/jquery.min.js')
					    ->appendFile('/bower_components/lodash/dist/lodash.compat.min.js')
					    ->appendFile('/bower_components/masonry/dist/masonry.pkgd.min.js')
					    ->appendFile('/js/jquery.bxslider.min.js')
					    ->appendFile('/js/cache.js')
					    ->appendFile('/js/overlay.js')
                        ->appendFile('/js/constructor.js')
                        ->appendFile('/js/preview.js')
                        ->appendFile('/js/main.js');

		// array('rel' => 'icon', 'href' => '/images/favicon.ico', 'type' => 'image/x-icon') // favicon
		$viewLayout->headLink()
						->appendStylesheet('/css/default_redactor.css')
						->appendStylesheet('/css/main.css')
						->appendStylesheet('http://fonts.googleapis.com/css?family=Open+Sans:300,400')
						->appendStylesheet('http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic');

		$view->_view = $viewLayout;
		$view->_layout = Zend_Layout::getMvcInstance();

		/*
		 *	LANGUAGES
		 */
		$language = new Model_Language();
		$view->_languages = $language->listActive();

		return $view;
	}

	protected function _initTranslation()
	{
        $translate = new Zend_Translate(
        	array(
        		'adapter' => 'csv',
        		'disableNotices' => true,
        	)
        );

        $translate->setLocale( $_COOKIE['_zigzag_language'] ? $_COOKIE['_zigzag_language'] : 'lv' );

		$registry = Zend_Registry::getInstance();
		$registry->set('Zend_Translate', $translate);
	}

	protected function _initNavigation()
	{
		// LOAD NAVIGATION FROM XML
	    $config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/navigation_default.xml', 'navigation');

	    $navigation = new Zend_Navigation($config);

		Zend_Registry::set('Zend_Navigation', $navigation);
	}

	protected function _initCache()
	{
		$frontend = array(
				'lifetime' => 7200,
				'automatic_serialization' => true
			);

		$backend = array(
				'cache_dir' => APPLICATION_PATH . '/cache',
			);

		$cache = Zend_Cache::factory(
			'Core',
			'File',
			$frontend,
			$backend
		);

//		Zend_Db_Table_Abstract::setDefaultMetadataCache($cache); //cache database table schemata metadata for faster SQL queries
		Zend_Registry::set('Cache', $cache);
	}
}
