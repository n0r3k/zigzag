<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
	public function adminInitFunction()
	{
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new Plugin_CheckLogin());

		// Smarty
		$path = APPLICATION_PATH . DS . 'modules' . DS . 'admin' . DS . 'views' . DS . 'scripts';

		$view = new Zend_View_Smarty($path);

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
		$viewRenderer->setViewBasePathSpec($view->_smarty->template_dir) 
            ->setViewScriptPathSpec(':controller/:action.:suffix') 
            ->setViewScriptPathNoControllerSpec(':action.:suffix') 
            ->setViewSuffix('phtml');                             
            
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		
		// Layout 
		$viewLayout = new Zend_View();
		$viewLayout->doctype('HTML5');
		$viewLayout->headTitle('Admin - ZigZag Factory');
		$viewLayout->headMeta()->setCharset('UTF-8')
						->setHttpEquiv('X-UA-Compatible', 'IE=edge,chrome=1');
		$viewLayout->headScript()->appendFile('/js/jquery-1.7.2.min.js', 'text/javascript')
						->appendFile('/js/jquery-ui-1.8.21.min.js', 'text/javascript')
						->appendFile('/js/jquery.tablesorter.min.js', 'text/javascript')
						->appendFile('/js/jquery.equalHeight.js', 'text/javascript')
						->appendFile('/js/redactor.min.js', 'text/javascript')
						->appendFile('/js/jquery.form.js', 'text/javascript')
						->appendFile('/js/jquery.colorbox-min.js', 'text/javascript');
		// array('rel' => 'icon', 'href' => '/images/favicon.ico', 'type' => 'image/x-icon') // favicon
		$viewLayout->headLink()->appendStylesheet('/css/redactor.css')
						->appendStylesheet('/css/jquery-ui-smoothness.css')
						->appendStylesheet('/css/admin_colorbox.css');

		$view->_view 	= $viewLayout;
		$view->_layout 	= Zend_Layout::getMvcInstance();

        return $view;
	}
	
	public function adminInitNavigation()
	{
		// LOAD NAVIGATION FROM XML	
	    $config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/navigation_admin.xml', 'navigation');

	    $navigation = new Zend_Navigation($config);

		Zend_Registry::set('Zend_Navigation', $navigation);
	}
	
	public function adminInitAcl()
	{
		$acl = new Zend_Acl();
		 
		$acl->addRole(new Zend_Acl_Role('user'))
		    ->addRole(new Zend_Acl_Role('superuser'), 'user')
		    ->addRole(new Zend_Acl_Role('admin'), 'superuser');
		    
		$acl->add(new Zend_Acl_Resource('index'))
			->add(new Zend_Acl_Resource('login'))
			->add(new Zend_Acl_Resource('error'))
			->add(new Zend_Acl_Resource('page'))
			->add(new Zend_Acl_Resource('user'))
			->add(new Zend_Acl_Resource('classifier'))
			->add(new Zend_Acl_Resource('gallery'));
//			->add(new Zend_Acl_Resource('order'));

		// user
		$acl->allow('user', 'index');
		$acl->allow('user', 'login');
		$acl->allow('user', 'error');
		$acl->deny('user', 'user', 'edit');
		$acl->deny('user', 'classifier');
		// superuser
		$acl->allow('superuser', 'user');
		$acl->allow('superuser', 'user', 'edit');
		$acl->allow('superuser', 'classifier');
		$acl->allow('superuser', 'page');
			
		Zend_Registry::set('Zend_Acl', $acl);
		
		return $acl;	
	}
}