<?php

class Admin_PageController extends Ideo_Controller_Action {

	public function clear_tags($str)
	{
	    return strip_tags($str, '<code><span><div><label><a><br><p><b><i><del><strike><u><img><video><audio><iframe><object><embed><param><blockquote><mark><cite><small><ul><ol><li><hr><dl><dt><dd><sup><sub><big><pre><code><figure><figcaption><strong><em><table><tr><td><th><tbody><thead><tfoot><h1><h2><h3><h4><h5><h6>');
	}
		
	public function indexAction()
	{
		// Request data
		$request = $this->getRequest();
		$id = $request->getParam('id');
		$this->view->page_code = $id;

		// Get lang
		$lang = $request->getParam('lang', 'lv');
		$this->view->lang = $lang;

		// Get version
		$version = $request->getParam('version');
		$this->view->version = $version;

		// Get page 
		$page = new Model_Page($id, $lang, $version);
		$this->view->page = $page->data;

		// exit if no data
		if (!$page->getId()) {
			throw new Exception('Page "' . $id . '" not found!');
		}

		// Images
		$image = new Model_Image();
		$this->view->image_list = $image->listItems($request->getParam('controller'), $page->data['ppag_id']);
		
		// Language list
		$lang_list = new Model_Language();
		$this->view->lang_list = $lang_list->listItems();
		
		// Get history
		$history = $page->fetchHistory();
		$this->view->history = $history;

		$page_lang = new Model_PageLanguage();

		// Save
		if($request->getParam('save'))
		{
			$data = $request->getParams();
			$data_lang['lpag_ppag_id'] = $page->getId();
			$data_lang['lpag_sysu_id'] = $this->user_info['user_id'];
			$data_lang['lpag_clsl_code'] = $lang;
			$data_lang['lpag_text'] = trim($this->clear_tags(get_magic_quotes_gpc()
												? stripslashes($data['lpag_text'])
												: $data['lpag_text']));
			$data_lang['lpag_name'] = $data['lpag_name'];
			$data_lang['lpag_slug'] = $this->toSlug($data['lpag_name']);
			
			$page_lang->save($data_lang);
//			$page->save($data);		

			$this->_redirect('/admin/page/' . $id . ($version ? ('/version/' . $version) : '') . ($lang != 'lv' ? ('/lang/' . $lang) : ''));
		}		
	}	
}