<?php

class Admin_FeedbackController extends Ideo_Controller_Action {

	protected $lang;
		
	public function indexAction()
	{
		$feedback = new Model_Feedback();
		$feedback_list = $feedback->listItems();
		
		if ($feedback_list)
		{
			foreach($feedback_list as &$v)
			{
				$image = new Model_Image();
				$v['first_image'] = $image->getFirst('feedback', $v['pfbk_id']);
			}
		}
		
		$this->view->feedback_list = $feedback_list;		
	}
		
	public function editAction()
	{
		// Request data
		$request = $this->getRequest();

		if($request->getParam('cancel'))
		{
			$this->_redirect('/admin/' . $request->getParam('controller'));			
		}

		$id = $request->getParam('id');
		$this->view->id = $id;

		// Get lang
		$this->lang = $request->getParam('lang', 'lv');
		$this->view->lang = $this->lang;

		// Get feedback
		$feedback = new Model_Feedback($id);
		$this->view->feedback = $feedback->data;
		
		// Feedback language data
		$feedback_lang = new Model_FeedbackLanguage($id, $this->lang);
		$this->view->feedback_lang = $feedback_lang->data;

		// Images
		$image = new Model_Image();
		$this->view->image_list = $image->listItems($request->getParam('controller'), $id);

		if($id && empty($feedback_lang->data_all))
		{
			$this->_redirect('/admin/' . $request->getParam('controller'));
		}

		// Language list
		$lang_list = new Model_Language();
		$this->view->lang_list = $lang_list->listItems();

		// Save
		if($request->getParam('save') || $request->getParam('switch_lang'))
		{
			if($id)
			{
				$feedback->data['pfbk_id'] = $id;
				$feedback->save($feedback->data);
			}
			else
			{
				$id = $feedback->save($feedback->data);
			}
			
			unset($feedback_lang->data['lfbk_id']);
			$feedback_lang->data['lfbk_pfbk_id'] 	= $id;
			$feedback_lang->data['lfbk_clsl_code'] 	= $request->getParam('lfbk_clsl_code');
			$feedback_lang->data['lfbk_text'] 		= trim($request->getParam('lfbk_text'));

			if ($id)
			{
				$feedback_lang->save($feedback_lang->data);
			}
			else
			{
				$id = $feedback_lang->save($feedback_lang->data);
			}
			
			if($request->getParam('switch_lang'))
			{
				$this->lang = $request->getParam('switch_lang');
			}

			$this->_redirect('/admin/' . $request->getParam('controller') . '/edit/' . $id . ($this->lang != 'lv' ? ('/lang/' . $this->lang) : ''));				
		}		
	}
	
	public function deleteAction()
	{
		$request = $this->getRequest();
		$id = $request->getParam('id');

		if($id)
		{
			// Feedback language
			$feedback_lang = new Model_FeedbackLanguage($id);
			$feedback_lang->deleteItem();
			
			// Feedback
			$feedback = new Model_Feedback($id);
			$feedback->deleteItem();
			
			// images
			$image = new Model_Image();
			$image_list = $image->listItems('feedback', $id);

			if(!empty($image_list))
			{
				foreach($image_list as $v)
				{
					$tmp = new Model_Image($v['pimg_id']);
					$tmp->deleteItem();
				}	
			}
			
			$this->_redirect('/admin/feedback');	
		}
	}	
}