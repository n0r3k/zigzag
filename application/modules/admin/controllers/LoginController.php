<?php
    
class Admin_LoginController extends Ideo_Controller_Action
{
    public function getAuthAdapter(array $params)
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('sys_users')
                    ->setIdentityColumn('sysu_username')
                    ->setCredentialColumn('sysu_password');
                   
        $authAdapter->setIdentity($params['username'])
        			->setCredential('*' . sha1(sha1($params['password'], true)));
        			
        return $authAdapter;
    }
    
    public function indexAction()
    {              
	    // if logged in, don't show login screen
        $auth    = Zend_Auth::getInstance();
        if($auth->hasIdentity())
        {
        	$this->_redirect('/admin/');
    	}
    }

    public function processAction()
    {
        $request = $this->getRequest();

        // Get our authentication adapter and check credentials
        $adapter = $this->getAuthAdapter($request->getParams());

        try {
	        $auth    = Zend_Auth::getInstance();
	        
	        // authenticate and get user information from db
	       	$result = $auth->authenticate($adapter);
	        $result_data = $adapter->getResultRowObject();
	                
	        // store user info in session
	        $oSession = new Zend_Session_Namespace('Zend_Auth');       
	        $oSession->user_id 			= $result_data->sysu_id;
	        $oSession->username			= $result_data->sysu_username;
	        $oSession->user_name 		= $result_data->sysu_name;
	        $oSession->user_surname		= $result_data->sysu_surname;
	        $oSession->user_email 		= $result_data->sysu_email;
	        $oSession->user_is_active 	= $result_data->sysu_is_active;
	        $oSession->user_is_admin 	= $result_data->sysu_is_admin;	                
	    } catch (Exception $e) {
		    // error
	    }

	    $this->_redirect('/admin');
    }

    public function logoutAction()
    {                            
        Zend_Auth::getInstance()->clearIdentity();
        $oSession = new Zend_Session_Namespace('Zend_Auth');
        $oSession->unsetAll();       
        $this->_redirect('/admin/'); // back to login page
    }
}