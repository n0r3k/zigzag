<?php

class Admin_NavigationController extends Ideo_Controller_Action {
	
	protected $lang;	
	
	public function indexAction()
	{
		// Request data
		$request = $this->getRequest();

		$code = $request->getParam('id');
		$this->view->code = $code;

		// Get lang
		$this->lang = $request->getParam('lang', 'lv');
		$this->view->lang = $this->lang;

		// Get navigation
		$navigation = new Model_Navigation($code, $this->lang);
		$this->view->navigation = $navigation->data;

		// Language list
		$lang_list = new Model_Language();
		$this->view->lang_list = $lang_list->listItems();

		// Save
		if($request->getParam('save') || $request->getParam('switch_lang'))
		{
			// Navigation language data
			$navigation_lang = new Model_NavigationLanguage();
			
			$navigation_lang->data['lnav_id']				= $navigation->data['lnav_id'];
			$navigation_lang->data['lnav_pnav_id']			= $navigation->getId();
			$navigation_lang->data['lnav_title'] 			= $request->getParam('lnav_title');
			$navigation_lang->data['lnav_meta_keywords'] 	= $request->getParam('lnav_meta_keywords');
			$navigation_lang->data['lnav_meta_description'] = $request->getParam('lnav_meta_description');
			$navigation_lang->data['lnav_clsl_code']		= $request->getParam('lnav_clsl_code');
			
			$navigation_lang->save($navigation_lang->data);		

			if($request->getParam('switch_lang'))
			{
				$this->lang = $request->getParam('switch_lang');
			}

			$this->_redirect('/admin/' . $request->getParam('controller') . '/' . $code . ($this->lang != 'lv' ? ('/lang/' . $this->lang) : ''));				
		}		
	}	
}