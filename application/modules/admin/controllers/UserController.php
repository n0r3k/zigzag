<?php

class Admin_UserController extends Ideo_Controller_Action {
	
	public function indexAction() {
		$users = new Model_User();
		$accounts = $users->listUsers();
		
		$this->view->user_list = $accounts;
	}
	
	public function editAction() {
		$request = $this->getRequest();
		
		if($request->getParam('cancel'))
		{
			$this->_redirect('/admin/user');			
		}

		$id = $request->getParam('id');

		$user = new Model_User($id);

		$this->view->user = $user->data;

		if($request->getParam('save'))
		{
			$user->data['sysu_username'] 	= $request->getParam('sysu_username');
			$user->data['sysu_name'] 		= $request->getParam('sysu_name');
			$user->data['sysu_surname']		= $request->getParam('sysu_surname');
			$user->data['sysu_email']		= $request->getParam('sysu_email');
			$user->data['sysu_is_admin']	= $request->getParam('sysu_is_admin');
			$user->data['sysu_is_active']	= $request->getParam('sysu_is_active') ? 1 : 0;
			
			$password1 = $request->getParam('sysu_password1');
			$password2 = $request->getParam('sysu_password2');
			
			if(($password1 && $password2) && $password1 == $password2)
			{
				$user->data['sysu_password'] = $password1;				
				$id = $user->save($user->data);
			}
			elseif($id)
			{
				$id = $user->save($user->data);
			}
			
			if($id)
			{
				$this->_redirect('/admin/user/edit/id/'.$id);				
			}
			else
			{
				$this->view->user = $user->data;
			}
		}			
	}
	
	public function deleteAction()
	{
		$request = $this->getRequest();
		$id = $request->getParam('id');

		if($id)
		{
			$user = new Model_User($id);
			$user->deleteUser();
			
			$this->_redirect('/admin/user');	
		}		
	}
}