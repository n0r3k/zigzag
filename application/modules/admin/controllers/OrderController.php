<?php

class Admin_OrderController extends Ideo_Controller_Action 
{
	public function indexAction() {
		// Order status
		$order_status = new Model_OrderStatus();
		$this->view->order_status_list = $order_status->listItems();

		// Orders
		$order = new Model_Order();
		$this->view->order_list = $order->listItems();
	}

	public function invoiceAction()
	{
		$request = $this->getRequest();
		$order_id = $request->getParam('id');
		$is_invoice = $request->getParam('is_invoice', 1);
		$this->view->is_invoice = $is_invoice;
		
		// Order
		$order = new Model_Order($order_id);
		$order->data['pord_invoice_total_lvl'] = number_format($order->data['pord_invoice_total'] * Model_Cart::EUR_TO_LVL, 2);
		$order->data['pord_invoice_total_words'] = Ideo_NumberConversion::numberToWords($order->data['pord_invoice_total'], $request->getParam('language', 'lv'), 'EUR');
		$this->view->order = $order->data;

		// Order items
		$order_items = new Model_OrderItems($order_id);

		$katalogs_orders = $order_items->listItems('lv');
		$darbnica_orders = $order_items->listFactoryItems();
		
		$this->view->order_items = array_merge($katalogs_orders, $darbnica_orders);
		
		// PDF config		
		require_once(ROOT_PATH . '/library/Ideo/Dompdf/dompdf_config.inc.php');		
		spl_autoload_register('DOMPDF_autoload'); 		

		// Disables skeleton layout
	    $this->_helper->layout()->disableLayout();
	    // Disables action view rendering
	    $this->_helper->viewRenderer->setNoRender(true);

		$html = $this->view->render(APPLICATION_PATH . '/modules/admin/views/scripts/order/invoice.phtml');		

		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		
		$dompdf->render();
		$dompdf->stream('invoice-' . $order->data['pord_invoice_number'] . '.pdf', array('Attachment' => 0));		
	}
	
	public function viewAction() {
		// same as edit
		self::editAction();
	}
		
	public function editAction() {
		$request = $this->getRequest();
		$id = $request->getParam('id');
		
		if($request->getParam('cancel') || !$id)
		{
			$this->_redirect('/admin/' . $request->getParam('controller') . '/view/' . $id);			
		}
		
		if ($request->getParam('edit'))
		{
			$this->_redirect('/admin/' . $request->getParam('controller') . '/edit/' . $id);			
		}

		$this->view->id	= $id;

		$new_status = false;

		// Classifiers
		// - country
		$country = new Model_Countries();
		$country_list = $country->listActive();

		$this->view->country_list = $country_list;

		// Order data
		$order = new Model_Order($id);
		$this->view->order = $order->data;

		$order_items = new Model_OrderItems($id);
		
		$katalogs_orders = $order_items->listItems('lv');
		$darbnica_orders = $order_items->listFactoryItems();
				
		$order_item_list = array_merge($katalogs_orders, $darbnica_orders);

		$pfac_id = false;

		if ($order_item_list)
		{
			foreach ($order_item_list as &$item)
			{
				if (!empty($item['poit_pgal_id']))
				{
					$image = new Model_Image();
					$item['first_image'] = $image->getFirst('gallery', $item['pgal_id']);			
				}
				
				if (!$pfac_id)
				{
					$pfac_id = $item['poit_pfac_id'];
				}
			}
				
			if ($pfac_id)
			{
				$factory = new Model_FactoryItems($pfac_id);
				$factory_items = $factory->listItems();
	
				if ($factory_items)
				{
					foreach ($factory_items as &$f_item)
					{
						$f_item['pfai_content'] = unserialize($f_item['pfai_content']);
						$f_item['option_aa_name'] = $factory->options[$f_item['pfai_option_aa']];
					}
				}
	
				$this->view->factory_items = $factory_items;
			}
		}

		$this->view->order_items = $order_item_list;

		if($id && empty($order->data))
		{
			$this->_redirect('/admin/' . $request->getParam('controller'));
		}

		// status change
		if ( $request->getParam( 'approve' ) )
		{
			$new_status = Model_OrderStatus::ORDER_STATUS_CODE_APPROVED;
		}

		if ( $request->getParam( 'complete' ) )
		{
			$new_status = Model_OrderStatus::ORDER_STATUS_CODE_COMPLETED;
		}

		if ( $request->getParam( 'cancel' ) )
		{
			$new_status = Model_OrderStatus::ORDER_STATUS_CODE_CANCELED;
		}

		if ( $new_status )
		{
			$order->data['pord_cost_id'] = Model_OrderStatus::getIdByCode( $new_status );
			$order->save( $order->data );

			$this->_redirect( '/admin/' . $request->getParam( 'controller' ) . '/view/' . $id );				
		}
				
		if ( $request->getParam( 'save' ) )
		{
			// billing
			$order->data['pord_cl_first_name'] = $request->getParam( 'pord_cl_first_name' );		
			$order->data['pord_cl_last_name'] = $request->getParam( 'pord_cl_last_name' );		
			$order->data['pord_cl_email'] = $request->getParam( 'pord_cl_email' );		
			$order->data['pord_cl_newsletter'] = $request->getParam( 'pord_cl_newsletter' ) ? 1 : 0; // todo : newsletter table, unsubscribe support
			$order->data['pord_cl_phone'] = $request->getParam( 'pord_cl_phone' );
			$order->data['pord_cl_additional_info'] = $request->getParam( 'pord_cl_additional_info' );
			// delivery
			$order->data['pord_cl_delivery_name'] = $request->getParam( 'pord_cl_delivery_name' );
			$order->data['pord_cl_delivery_phone'] = $request->getParam( 'pord_cl_delivery_phone' );

			$order->data['pord_cl_delivery_country'] = $request->getParam( 'pord_cl_delivery_country' );
			$order->data['pord_cl_delivery_city'] = $request->getParam( 'pord_cl_delivery_city' );
			$order->data['pord_cl_delivery_street'] = $request->getParam( 'pord_cl_delivery_street' );
			$order->data['pord_cl_delivery_zip'] = $request->getParam( 'pord_cl_delivery_zip' );
			
			$order->save( $order->data );

			$this->_redirect( '/admin/' . $request->getParam( 'controller' ) . '/view/' . $id );				
		}
	}
}