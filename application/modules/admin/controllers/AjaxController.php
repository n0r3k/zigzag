<?php

class Admin_AjaxController extends Ideo_Controller_Action {

	public function preDispatch() 
	{
	    $this->_helper->layout()->disableLayout(); 
	    $this->_helper->viewRenderer->setNoRender(true);
	}

	public function uploadAction() 
	{
		$request = $this->getRequest();
		$img_ref_id = $request->getParam('ref_id');
		$img_ref_type = strtolower($request->getParam('ref_type'));
		
		$errors = $data = array();

		$upload = new Zend_File_Transfer_Adapter_Http();
		$uploads_dir = ROOT_PATH . '/public/uploads/' . $img_ref_type . 'images/' . $img_ref_id . DS;

		if(!file_exists($uploads_dir) || !is_dir($uploads_dir)) 
		{
			mkdir($uploads_dir, 0777, true); 	// Recursive permissions.
		}
		
		foreach ($upload->getFileInfo() as $file => $info) 
		{	
		    if($upload->isUploaded($file)) 
		    {	
//		    	$upload->addValidator('IsImage', false)
//					   ->addValidator('Size', false, 3000000);

/*				
		    	if($img_ref_type == 'gallery')
		    	{
					$upload->addValidator(
						'ImageSize',
						false,
						array(
							'minwidth' => Model_Image::$image_sizes[$img_ref_type . '_img_width'],
							'minheight' => Model_Image::$image_sizes[$img_ref_type . '_img_height']
						)
					);
				}
*/				
		        // $name   = $upload->getFileName($file);
				$ext = strrchr($info['name'], '.');
				$new_name = uniqid() . $ext;
		        $fname  = $uploads_dir . $new_name;

		        $upload->addFilter('Rename', array('target' => $fname, 'overwrite' => true));

				if ($upload->isValid($file))
				{
					try 
					{
						$upload->receive($file);
						$image = new Model_Image();
						$data[$file]['id'] = $image->createImages($new_name, $uploads_dir, $img_ref_id, $img_ref_type, $banner_img_type);
						$data[$file]['name'] = $new_name;
						$data[$file]['path'] = '/uploads/' . $img_ref_type . 'images/' . $img_ref_id . '/';
					} 
					catch (Zend_File_Transfer_Exception $e) 
					{
						$errors[$file][] = 'Bad image data: '.$e->getMessage();
					}
				}
				else {
					$errors[$file][] = 'Bad image data: ' . implode(',', $upload->getMessages());
				}
			}
		}

		$json['errors'] = $errors;
		$json['data'] = $data;
		echo json_encode($json);
		exit;
	}

	public function deleteAction() 
	{
		$request = $this->getRequest();
		$delete_item = $request->getParam('delete_item');

		switch($delete_item)
		{
			case 'image':
				$img_id = $request->getParam('img_id');

				$model = new Model_Image($img_id);

				$result = $model->deleteItem();
				
				if(!$result)
				{
					echo $result;
				}
			break;

			case 'stable_type_attrib':
				$attrib_id = $request->getParam('attrib_id');
				$model = new Model_StableTypeAttribute($attrib_id);

				$model->deleteItem();
			break;
			
			case 'stable_type_attrib_val':
				$attrib_id = $request->getParam('attrib_id');
				$model = new Model_StableAttributes($attrib_id);
				$model->data['psta_id'] = $attrib_id;
				
				$model->deleteItem($model->data);
			break;
		}
		
	}

	public function orderAction() 
	{
		$request = $this->getRequest();
		$order_item = $request->getParam('order_item');

		switch($order_item) 
		{
			case 'cls_countries':
				$order = $request->getParam('cls_countries');
				for ( $i = 0 ; $i < count($order) ; $i++ ) {
					$model = new Model_Countries($order[$i]);
					$model->setOrder($i);
				}

			case 'cls_gallery_categories':
				$order = $request->getParam('cls_gallery_categories');
				for($i=0; $i<count($order); $i++) {
					$model = new Model_GalleryCategories($order[$i]);
					$model->setOrder($i);
				}
			break;
		}
	}

	public function changestateAction()
	{
		$request = $this->getRequest();
		$type = $request->getParam('type');
		$state = $request->getParam('state');
		$ref_id = $request->getParam('ref_id');
		
		switch($type) {
			case 'gallery':
				$gallery = new Model_Gallery($ref_id);
				$gallery->data['pgal_is_active'] = $state;
				$gallery->save($gallery->data);
			break;
			
			case 'feedback':
				$feedback = new Model_Feedback($ref_id);
				$feedback->data['pfbk_is_active'] = $state;
				$feedback->save($feedback->data);
			break;
		}
	}
	
	public function saveentryAction()
	{
		$request 	= $this->getRequest();
		$code 		= $request->getParam('code');
		$id 		= $request->getParam('id');
		$name 		= $request->getParam('value');

		if(empty($name))
		{
			return false;
		}

		switch($code)
		{
			case 'cls_gallery_categories':
				$prefix = 'ccat_';
				$classifier =  new Model_GalleryCategories($id);
				break;
			default : return;
		}
		
		$classifier->data[$prefix . 'name'] = $name;
		
		$id = $classifier->data[$prefix . 'id']; 
		
		if(!empty($id))
		{
			$classifier->save($classifier->data);
		}
		else
		{
			$id = $classifier->save($classifier->data);
		}
		
		echo $id;
	}

	public function deleteentryAction()
	{
		$request 	= $this->getRequest();
		$code 		= $request->getParam('code');
		$id 		= $request->getParam('id');

		if(empty($id))
		{
			return false;
		}
				
		switch($code)
		{
			case 'cls_gallery_categories': 
				$prefix = 'ccat_'; 
				$classifier = new Model_GalleryCategories($id); 
				break;
			default : return;
		}
		
		return $classifier->deleteItem();
	}
}