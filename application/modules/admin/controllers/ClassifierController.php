<?php

class Admin_ClassifierController extends Ideo_Controller_Action {
	protected $id;
	protected $code;
	protected $lang;
	protected $request;
	protected $classifier;
	protected $classifier_lang;
	protected $classifier_list = array(
		array(
			'label' => 'Valodas',
			'code'	=> 'cls_languages'
		),
		array(
			'label' => 'Kolekcijas',
			'code'	=> 'cls_gallery_categories'
		),
		array(
			'label' => 'Izmēri',
			'code'	=> 'cls_sizes'
		),
		array(
			'label' => 'Valstis',
			'code' 	=> 'cls_countries'
		)
	);

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function getLang()
	{
		return $this->lang;
	}

	public function setLang($lang)
	{
		$this->lang = $lang;
	}

	public function init()
	{
		parent::init();
		
		$this->request = $this->getRequest();
		
		$this->setId($this->request->getParam('id'));
		$this->setCode($this->request->getParam('code'));
		$this->setLang($this->request->getParam('lang', 'lv'));

		$this->view->lang = $this->getLang();

		if($this->getCode())
		{
			switch($this->getCode())
			{
				case 'cls_languages' 			: $this->classifier = new Model_Language($this->getId()); break;
				case 'cls_gallery_categories' 	: $this->classifier = new Model_GalleryCategories($this->getId()); break;
				case 'cls_sizes'				: $this->classifier = new Model_Size($this->getId()); break;
				case 'cls_countries'			: $this->classifier = new Model_Countries($this->getId()); break;
				
/*
				case 'cls_stable_types' : 
					$this->classifier = new Model_StableType($this->getId()); 
					$this->classifier_lang = new Model_StableTypeLanguage();
					break;
*/
				default : $this->_redirect('/admin/classifier');
			}
			
			$this->view->code = $this->getCode();
		}	
	}

	public function indexAction() {
		$this->view->classifier_list = $this->classifier_list;
	}
	
	public function listAction() {
		$this->view->classifier = $this->classifier->listItems();		
	}
		
	public function editAction() {
		if($this->request->getParam('cancel'))
		{
			$this->_redirect('/admin/classifier/list/code/'.$this->getCode());			
		}			

		// load classifier data in smarty
		if($this->classifier->data)
		{
			$this->view->classifier = $this->classifier->data;
		}

		if($this->classifier->data_all)
		{
			$this->view->classifier_all = $this->classifier->data_all;
		}

		// - classifiers
		$language = new Model_Language();
		$this->view->lang_list = $language->listItems();

		$product_types = new Model_ProductTypes();
		$this->view->product_type_list = $product_types->listItems();					
		
		if($this->request->getParam('save') || $this->request->getParam('switch_lang'))
		{
			switch($this->getCode())
			{
				case 'cls_languages' : 		
					$this->classifier->data['clsl_name']	 	= $this->request->getParam('clsl_name');
					$this->classifier->data['clsl_code'] 		= $this->request->getParam('clsl_code');
					$this->classifier->data['clsl_order']		= $this->request->getParam('clsl_order');
					$this->classifier->data['clsl_is_default']	= $this->request->getParam('clsl_is_default') ? 1 : 0;
					$this->classifier->data['clsl_is_active']	= $this->request->getParam('clsl_is_active') ? 1 : 0;
					break;
				case 'cls_galley_categories' :
					$this->classifier->data['ccat_name'] 	= $this->request->getParam('ccat_name');
					$this->classifier->data['ccat_order'] 	= $this->request->getParam('ccat_order');
					break;
				case 'cls_sizes' :
					$this->classifier->data['clss_code']	= $this->request->getParam('clss_code');
					$this->classifier->data['clss_width']	= $this->request->getParam('clss_width');
					$this->classifier->data['clss_height']	= $this->request->getParam('clss_height');
					$this->classifier->data['clss_units']	= $this->request->getParam('clss_units');			
					break;
				case 'cls_countries' :
					$this->classifier->data['clsc_code'] 	= $this->request->getParam('clsc_code');
					$this->classifier->data['clsc_name_lv'] = $this->request->getParam('clsc_name_lv');
					$this->classifier->data['clsc_name_en'] = $this->request->getParam('clsc_name_en');
					$this->classifier->data['clsc_name_ru'] = $this->request->getParam('clsc_name_ru');
					$this->classifier->data['clsc_name_no'] = $this->request->getParam('clsc_name_no');
					$this->classifier->data['clsc_order'] 	= $this->request->getParam('clsc_order');
					$this->classifier->data['clsc_is_active'] 	= $this->request->getParam('clsc_is_active') ? 1 : 0;
					break;
				case 'cls_stable_types' :
					$form_data = $this->request->getParam('cstt');
									
					if(empty($form_data['lv']['lstt_name']))
					{
						break;
					}

					// save type data
					$this->classifier->data['cstt_color'] = $this->request->getParam('cstt_color') ? $this->request->getParam('cstt_color') : '#999999';
					$this->classifier->data['cstt_show_on_map'] = $this->request->getParam('cstt_show_on_map') ? 1 : 0;
																				
					$id = $this->classifier->save($this->classifier->data);

					if(!$this->getId())
					{
						$this->setId($id);
					}
									
					// save stable data		
					foreach($this->classifier->data_all as $data)
					{
						$lang = $data['clsl_code'];
						
						if($lang && !empty($form_data[$lang]['lstt_name']))
						{
							$data['lstt_clsl_code'] = $lang;

							$data['lstt_id']		= $form_data[$lang]['lstt_id'];							
							$data['lstt_cstt_id'] 	= $this->getId();
 							$data['lstt_name'] 		= $form_data[$lang]['lstt_name'];
							$data['lstt_slug'] 		= $this->toSlug($form_data[$lang]['lstt_name']);
							
							$this->classifier_lang->save($data);
						}
						else
						{
							$this->classifier_lang->deleteItem($data);
						}
					}

					// save attributes
					$form_attribs = $this->request->getParam('attribs');

					if(!empty($form_attribs))
					{
						foreach($form_attribs as $v)
						{
							if(!empty($v['lsta_name']))
							{							
								$save = array();
								$save['csta_id'] 		= $v['csta_id'];
								$save['csta_cstt_id'] 	= $this->getId();
								$save['csta_type'] 		= $v['csta_type'];
								
								$save['lsta_id']		= $v['lsta_id'];
								$save['lsta_name'] 		= $v['lsta_name'];
								$save['lsta_clsl_code'] = $this->getLang();
	
								$attrib_id = $attribs->save($save);

								// save attrib language
								$save['lsta_csta_id'] = $v['csta_id'] ? $v['csta_id'] : $attrib_id;
	
								$attrib_lang = new Model_StableTypeAttribLanguage();
								$attrib_lang->save($save);
							}
						}
					}
					break;

				default : $this->_redirect('/admin/classifier');
			}
			
			// save classifier data					
//			if(!in_array($this->getCode(), array('cls_stable_types', 'cls_advert_category', 'cls_advert_section')))
			{
				$id = $this->classifier->save($this->classifier->data);
			}
			
			if(!$this->getId())
			{
				$this->setId($id);
			}

			if($this->request->getParam('switch_lang'))
			{
				$this->setLang($this->request->getParam('switch_lang'));
			}
										
			// redirect to saved data or populate form in case of error					
			$this->_redirect('/admin/classifier/edit/code/'.$this->getCode().'/id/'.$this->getId().'/lang/'.$this->getLang());				
		}			
	}
	
	public function deleteAction() {
		$this->classifier->deleteItem();

		$this->_redirect('/admin/classifier/list/code/'.$this->getCode());
	}	
}