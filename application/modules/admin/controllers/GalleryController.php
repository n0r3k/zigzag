<?php

class Admin_GalleryController extends Ideo_Controller_Action {
	
	protected $lang;
		
	public function indexAction() {
		// Gallery categories
		$gallery_categories = new Model_GalleryCategories();
		$this->view->gallery_categories = $gallery_categories->listItems();

		// Galleries
		$gallery = new Model_Gallery();
		$gallery_list = $gallery->listItems();

		if ($gallery_list)
		{
			foreach($gallery_list as &$v)
			{
				$image = new Model_Image();
				$v['first_image'] = $image->getFirst('gallery', $v['pgal_id']);
			}
		}
		
		$this->view->gallery_list = $gallery_list;
	}
		
	public function editAction() {
		$request = $this->getRequest();
		
		if($request->getParam('cancel'))
		{
			$this->_redirect('/admin/gallery');			
		}

		$id = $request->getParam('id');
		$this->view->id	= $id;

		$this->lang = $request->getParam('lang', 'lv');
		$this->view->lang = $this->lang;

		// Gallery data
		$gallery = new Model_Gallery($id);
		$this->view->gallery = $gallery->data;

		// Gallery language data
		$gallery_lang = new Model_GalleryLanguage($id, $this->lang);
		$this->view->gallery_lang = $gallery_lang->data;

		if($id && empty($gallery_lang->data_all))
		{
			$this->_redirect('/admin/gallery');
		}

		// Gallery categories
		$gallery_categories = new Model_GalleryCategories();
		$this->view->gallery_categories = $gallery_categories->listItems();
				
		// Images
		$image = new Model_Image();
		$this->view->image_list = $image->listItems($request->getParam('controller'), $id);

		// Language classifier
		$lang = new Model_Language();
		$this->view->lang_list = $lang->listItems();

		// Product types
		$pr_types = new Model_ProductTypes();
		$this->view->product_types = $pr_types->listItems();

		// Sizes
		$sizes = new Model_SizePrices();
		$this->view->size_list = $sizes->listItemsByGalleryId($id);

		if($request->getParam('save') || $request->getParam('switch_lang'))
		{
//			$gallery->data['pstb_slug_name'] 	= $request->getParam('pstb_slug_name');
//			$gallery->data['pstb_slug'] 		= $this->toSlug($request->getParam('pstb_slug_name'));

			$gallery->data['pgal_ccat_id']		= $request->getParam('pgal_ccat_id');
			$gallery->data['pgal_is_active']	= $request->getParam('gallery_state');
			
			if($id)
			{
				$gallery->data['pgal_id'] = $id;
				$gallery->save($gallery->data);
			}
			else
			{
				$id = $gallery->save($gallery->data);
			}

			unset($gallery_lang->data['lgal_id']);
			$gallery_lang->data['lgal_pgal_id'] 	= $id;
			$gallery_lang->data['lgal_clsl_code'] 	= $request->getParam('lgal_clsl_code');
			$gallery_lang->data['lgal_title'] 		= $request->getParam('lgal_title');
			$gallery_lang->data['lgal_description']	= $request->getParam('lgal_description');

			if($id)
			{
				$gallery_lang->save($gallery_lang->data);
			}
			else
			{
				$id = $gallery_lang->save($gallery_lang->data);
			}						

			// save size prices
			$post_sizes = $request->getParam('sizes');

			if ($post_sizes)
			{
				foreach ($post_sizes as $k => $v)
				{
					$size = new Model_SizePrices($id, $k);
					$size->data['xgls_pgal_id'] = $id;
					$size->data['xgls_clss_id'] = $k;

					if (!empty($v) && is_numeric($v))
					{
						$size->data['xgls_price'] = $v;
						$size->save($size->data); 
					}
					elseif (!empty($size->data['xgls_id']))
					{
						$size->deleteItem($size->data);
					}
				}
			}

			$gallery->updateMinPrice();

			if($request->getParam('switch_lang'))
			{
				$this->lang = $request->getParam('switch_lang');
			}

			$this->_redirect('/admin/' . $request->getParam('controller') . '/edit/' . $id . ($this->lang != 'lv' ? ('/lang/' . $this->lang) : ''));				
		}
	}
		
	public function deleteAction()
	{
		$request = $this->getRequest();
		$id = $request->getParam('id');

		if($id)
		{
			// sizes
			$sizes = new Model_SizePrices();
			$sizes->deleteItem(array('xgls_pgal_id' => $id), true);
			
			// images
			$image = new Model_Image();
			$image_list = $image->listItems('gallery', $id);

			if(!empty($image_list))
			{
				foreach($image_list as $v)
				{
					$img = new Model_Image($v['pimg_id']);
					$img->deleteItem();
				}	
			}
		
			// gallery lang	
			$gallery_lang = new Model_GalleryLanguage($id);
			$gallery_lang->deleteItem();			
		
			// gallery
			$gallery = new Model_Gallery($id);
			$gallery->deleteItem();
			
			$this->_redirect('/admin/gallery');	
		}
	}

	// updates all starting prices
	public function updatepricesAction()
	{
	    $this->_helper->layout()->disableLayout(); 
	    $this->_helper->viewRenderer->setNoRender(true);

		$tmp = new Model_Gallery();
		$gal_list = $tmp->listItems();
		
		if ($gal_list)
		{
			foreach ($gal_list as $g)
			{
				$gal = new Model_Gallery($g['pgal_id']);
				$gal->updateMinPrice();
			}
		}
	}
}