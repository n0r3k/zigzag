<?php

class Admin_FactoryController extends Ideo_Controller_Action {
		
	public function indexAction()
	{
		// Request data
		$request = $this->getRequest();
		$id = $request->getParam('id');
		$this->view->page_code = $id;

		// Save
		if($request->getParam('save'))
		{
			$data = $request->getParams();
			$data_lang['lpag_ppag_id'] = $page->getId();
			$data_lang['lpag_sysu_id'] = $this->user_info['user_id'];
			$data_lang['lpag_clsl_code'] = $lang;
			$data_lang['lpag_text'] = trim(get_magic_quotes_gpc()
												? stripslashes($data['lpag_text'])
												: $data['lpag_text']);
			$data_lang['lpag_name'] = $data['lpag_name'];
			$data_lang['lpag_slug'] = $this->toSlug($data['lpag_name']);
			
			$page_lang->save($data_lang);
//			$page->save($data);		

			$this->_redirect('/admin/page/' . $id . ($version ? ('/version/' . $version) : '') . ($lang != 'lv' ? ('/lang/' . $lang) : ''));
		}		
	}	
	
	public function editAction()
	{
			
	}
}