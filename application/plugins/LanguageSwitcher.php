<?php

class Plugin_LanguageSwitcher extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$registry = Zend_Registry::getInstance();
		$translate = $registry->get('Zend_Translate');

		$language = $request->getParam( 'language', $request->getCookie( '_zigzag_language' ) );

		if (!in_array($language, array('en', 'lv', 'ru')))
		{
			$language = 'lv';
		}
		
		$translate->setLocale($language);
		
		$registry->set('Zend_Translate', $translate);
	}
}