<?php

class Plugin_CheckLogin extends Zend_Controller_Plugin_Abstract
{
	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		if(!Zend_Auth::getInstance()->hasIdentity())
	    {
	    	if('admin' == $request->getModuleName() && 'login' != $request->getControllerName())
	    	{
				$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
				$redirector->gotoUrl('/admin/login')
						   ->redirectAndExit();		    
		    }
	    }
	}
}