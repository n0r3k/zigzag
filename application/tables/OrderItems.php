<?php

class Table_OrderItems extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_order_items';
	protected $_primary = 'poit_id';
	
	public function getName()
	{
		return $this->_name;
	}
}