<?php

class Table_Galleries extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_galleries';
    protected $_prefix	= 'pgal';
	protected $_primary = 'pgal_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}