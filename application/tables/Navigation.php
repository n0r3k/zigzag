<?php

class Table_Navigation extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_navigation';
    protected $_prefix	= 'pnav';
	protected $_primary = 'pnav_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}