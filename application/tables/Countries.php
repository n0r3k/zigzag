<?php

class Table_Countries extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_countries';
	protected $_primary = 'clsc_id';
	
	public function getName()
	{
		return $this->_name;
	}
}