<?php

class Table_Feedback extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_feedback';
    protected $_prefix	= 'pfbk';
	protected $_primary = 'pfbk_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}