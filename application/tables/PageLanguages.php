<?php

class Table_PageLanguages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'lng_pages';
	protected $_primary = 'lpag_id';
	
	public function getName()
	{
		return $this->_name;
	}
}