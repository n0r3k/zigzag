<?php

class Table_Sizes extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_sizes';
    protected $_prefix	= 'clss';
	protected $_primary = 'clss_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}