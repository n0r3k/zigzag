<?php

class Table_Languages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_languages';
	protected $_primary = 'clsl_id';
	
	public function getName()
	{
		return $this->_name;
	}
}