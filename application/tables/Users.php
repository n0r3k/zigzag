<?php

class Table_Users extends Zend_Db_Table_Abstract
{
    protected $_name    = 'sys_users';
	protected $_primary = 'sysu_id';
	
	public function getName()
	{
		return $this->_name;
	}
}