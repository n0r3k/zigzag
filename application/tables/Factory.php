<?php

class Table_Factory extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_factory';
	protected $_primary = 'pfac_id';
	
	public function getName()
	{
		return $this->_name;
	}
}