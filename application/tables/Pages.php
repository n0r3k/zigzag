<?php

class Table_Pages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_pages';
	protected $_primary = 'ppag_id';
	
	public function getName()
	{
		return $this->_name;
	}
}