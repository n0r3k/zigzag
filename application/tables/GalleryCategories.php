<?php

class Table_GalleryCategories extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_gallery_categories';
	protected $_primary = 'ccat_id';
	
	public function getName()
	{
		return $this->_name;
	}
}