<?php

class Table_Orders extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_orders';
	protected $_primary = 'pord_id';
	
	public function getName()
	{
		return $this->_name;
	}
}