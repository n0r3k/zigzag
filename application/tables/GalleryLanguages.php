<?php

class Table_GalleryLanguages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'lng_galleries';
    protected $_prefix	= 'lgal';
	protected $_primary = 'lgal_pgal_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}