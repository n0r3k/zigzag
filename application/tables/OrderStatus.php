<?php

class Table_OrderStatus extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_order_status';
	protected $_primary = 'cost_id';
	
	public function getName()
	{
		return $this->_name;
	}
}