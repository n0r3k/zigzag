<?php

class Table_AirpayStatusLogs extends Zend_Db_Table_Abstract
{
    protected $_name    = 'sys_airpay_status_logs';
    protected $_prefix	= 'slog';
	protected $_primary = 'slog_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}