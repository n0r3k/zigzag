<?php

class Table_FeedbackLanguages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'lng_feedback';
    protected $_prefix	= 'lfbk';
	protected $_primary = 'lfbk_pfbk_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}