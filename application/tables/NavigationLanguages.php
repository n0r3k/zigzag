<?php

class Table_NavigationLanguages extends Zend_Db_Table_Abstract
{
    protected $_name    = 'lng_navigation';
    protected $_prefix	= 'lnav';
	protected $_primary = 'lnav_pnav_id';
		
	public function getName()
	{
		return $this->_name;
	}
		
	public function getPrefix()
	{
		return $this->_prefix;
	}
}