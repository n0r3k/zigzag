<?php

class Table_ProductTypes extends Zend_Db_Table_Abstract
{
    protected $_name    = 'cls_product_types';
	protected $_primary = 'cprt_id';
	
	public function getName()
	{
		return $this->_name;
	}
}