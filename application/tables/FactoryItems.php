<?php

class Table_FactoryItems extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_factory_items';
	protected $_primary = 'pfai_id';
	
	public function getName()
	{
		return $this->_name;
	}
}