<?php

class Table_Images extends Zend_Db_Table_Abstract
{
    protected $_name    = 'pub_images';
	protected $_primary = 'pimg_id';
	
	public function getName()
	{
		return $this->_name;
	}
}