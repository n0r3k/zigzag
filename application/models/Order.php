<?php

class Model_Order
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	function __construct( $id = false )
	{
		$this->table = new Table_Orders();

		if ($id)
		{
	        $select = $this->table->select()
						->setIntegrityCheck( false )	// allows joins
						->from( array( 'pord' => $this->table->getName() ) )
						->joinLeft( array( 'clsc' => 'cls_countries' ), 'clsc.clsc_id = pord.pord_cl_delivery_country' )
						->where( 'pord.pord_id = ?', $id );

			if ( $row = $this->table->fetchRow( $select ) )
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}

    public function save()
    {
		$table=$this->table;
		// Validate fields here 

        $fields = $table->info( Zend_Db_Table_Abstract::COLS );

        foreach ( $this->data as $field => $value ) {
            if ( !in_array( $field, $fields ) ) {
                unset( $this->data[$field] );
            }
        }

		if (empty($this->id))
		{
			$result = $table->insert( $this->data );
			if ( $result > 0 )
				$this->id = $result;
				
			return $result;
		}
		else
			return $table->update( $this->data, 'pord_id = ' . $this->id );
    }
    
    public function listItems()
    {
        $select = $this->table->select()
						->from( array( 'pord' => $this->table->getName() ) )
						->order( array( 'pord.pord_timestamp ASC' ) );
		
		if ( $row = $this->table->fetchAll( $select ) )
		{
		    return $row->toArray();
		}
	}
	
	public function generateInvoiceNumber()
	{
		if (!$this->id)
		{
			return false;	
		}

		if ($this->data['pord_invoice_number'])
		{
			return $this->data['pord_invoice_number'];
		}
		
		$select = $this->table->select()
						->from( array( 'pord' => $this->table->getName() ), array( 'invoice_number' => 'IFNULL(MAX(pord.pord_invoice_number), 0) + 1' ) );

		if ( $row = $this->table->fetchRow( $select ) )
		{
			$result = $row->toArray();

			$this->data['pord_invoice_number'] = $result['invoice_number'];	
			
			if ( $this->save() )
				return $result['invoice_number']; 
		}
		
		return $this->data['pord_invoice_number'];
	}
}