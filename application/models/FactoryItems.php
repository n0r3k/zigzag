<?php

class Model_FactoryItems
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	public $options = array(
		1 => 'Praktisks atvērums kājgalī',
		2 => 'Caurspīdīgas spiedpogas kājgalī',
		3 => 'Ar aizloci uz iekšpusi, kājgalī',
		4 => 'Apstrādātas malas',
		5 => 'Gumija tikai stūros',
		6 => 'Gumija pa perimetru, matrača h-10 cm',
		7 => 'Gumija pa perimetru, matrača h-20 cm',
		8 => 'Gumija pa perimetru, matrača h-30 cm',
		9 => 'Gumija pa perimetru, matrača h-50 cm',
		10 => 'Gumija pa perimetru, matrača h-70 cm',
        11 => 'Ar aizloci uz iekšpusi, sānā',
        12 => 'Caurspīdīgas spiedpogas, sānā'
	);

	function __construct( $id = false )
	{
		$this->table = new Table_FactoryItems();

		if ($id)
		{
		    $this->id = $id;
		}
	}

    public function save()
    {
		$table=$this->table;
		// Validate fields here 

        $fields = $table->info( Zend_Db_Table_Abstract::COLS );

        foreach ( $this->data as $field => $value ) {
            if ( !in_array( $field, $fields ) ) {
                unset( $this->data[$field] );
            }
        }

        // insert only
		$result = $table->insert( $this->data );
		if ( $result > 0 )
			$this->id = $result;
			
		return $result;
    }
    
    public function listItems($lang = 'lv')
    {
        $select = $this->table->select()
						->setIntegrityCheck( false )	// allows joins
						->from( array( 'pfai' => $this->table->getName() ) )
						->joinLeft( array( 'clss' => 'cls_sizes' ), 'clss.clss_id = pfai.pfai_clss_id' )
						->joinLeft( array( 'cprt' => 'cls_product_types' ), 'cprt.cprt_id = clss.clss_cprt_id' )
						->where( 'pfai.pfai_pfac_id = ?', $this->id );
						
		if ( $row = $this->table->fetchAll( $select ) )
		{
		    return $row->toArray();
		}
	}
	
	public function deleteItems()
	{
		$select = $this->table->delete( array( 'pfai_pfac_id = ?' => $this->id ) );
		
		return $select;
	}
}