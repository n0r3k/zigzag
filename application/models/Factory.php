<?php

class Model_Factory
{
	// session properties
	protected $items = array();
	protected $prices = array();

	// table properties
    protected $table = null;
	protected $id = null;

	public $data = array();

	public function __construct( $id = false )
	{
		$factory = new Zend_Session_Namespace('Factory');
		$this->items 	= &$factory->items;		
		$this->prices 	= &$factory->prices;

		$this->table = new Table_Factory();

		if ($id)
		{
	        $select = $this->table->select()
						->setIntegrityCheck( false )	// allows joins
						->from( array( 'pfac' => $this->table->getName() ) )
						->where( 'pfac.pfac_id = ?', $id );

			if ( $row = $this->table->fetchRow( $select ) )
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}	

	// table procedures
    public function save()
    {
		$table=$this->table;
		// Validate fields here 

        $fields = $table->info( Zend_Db_Table_Abstract::COLS );

        foreach ( $this->data as $field => $value ) {
            if ( !in_array( $field, $fields ) ) {
                unset( $this->data[$field] );
            }
        }

		if (empty($this->id))
		{
			$result = $table->insert( $this->data );
			if ( $result > 0 )
				$this->id = $result;
				
			return $result;
		}
		else
			return $table->update( $this->data, 'pfac_id = ' . $this->id );
    }
    
	// session procedures
	public function saveLayout($type, $layout, $size, $bottom_color, $design_price, $option_aa)
	{
		$this->items[$type]['layout'] = $layout;		
		$this->items[$type]['bottom_color'] = $bottom_color;
		$this->items[$type]['design_price'] = $design_price;
		$this->items[$type]['aaId'] = $option_aa;
		$this->items[$type]['size'] = array('id' => $size);

		$m_size = new Model_Size($size);

		$this->items[$type]['size']['width'] = $m_size->data['clss_width'];
		$this->items[$type]['size']['height'] = $m_size->data['clss_height'];
		$this->items[$type]['size']['base_price'] = $m_size->data['clss_base_price'];
		$this->items[$type]['_total_price'] = number_format($design_price + $m_size->data['clss_base_price'], 2);

		$this->calcPrices();
	}

	public function loadLayout($type)
	{
		return isset($this->items[$type]) ? $this->items[$type] : false;		
	}

	public function calcPrices() 
	{
		if (!empty($this->items))
		{
			foreach ($this->items as $type => $item)
			{
				$this->prices['total'] += $item['_total_price'];
			}	
		}
	}

	public function getItems()
	{
		return $this->items;
	}

	public function getPrices()
	{
		return $this->prices;
	}

	public function getId()
	{
		return $this->id;
	}

	public function clear()
	{
		$this->items = array();
		$this->prices = array();
	}
}