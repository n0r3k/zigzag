<?php

class Model_Countries
{
    protected $table=null;
	protected $id = null;

	public $data = array();

	function __construct($id = false)
	{
		$this->table = new Table_Countries();

		if ($id)
		{
	        $select = $this->table->select()
//						->setIntegrityCheck(false)	// allows joins
						->from(array('clsc' => $this->table->getName()))
						->where('clsc.clsc_id = ?', $id);

			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
                
		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'clsc_id = '.$this->id);
    }
    
    public function listItems()
    {
        $select = $this->table->select()
						->from(array('clsc' => $this->table->getName()))
						->order(array('clsc.clsc_is_active DESC', 'clsc.clsc_order ASC', 'clsc.clsc_code ASC'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function listActive($lang = 'lv')
    {
        $select = $this->table->select()
						->from(array('clsc' => $this->table->getName()), array('clsc_id' => 'clsc.clsc_id', 'clsc_code' => 'clsc.clsc_code', 'clsc_name' => 'clsc.clsc_name_' . $lang))
						->where('clsc.clsc_is_active = 1') 
						->order(array('clsc.clsc_order ASC', 'clsc.clsc_name_' . $lang . ' ASC'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function deleteItem()
	{
		$result = $this->table->delete(array('clsc_id = ?' => $this->id));
		
		return $result;
	}
}