<?php

class Model_Image
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	public static $image_sizes = array(
		'full_size_prefix' => 'f_',
		
		'gallery_img_width' => 506,
		'gallery_img_height' => 506,
		'gallery' => array( 
			'large_thumb' => array(
				'prefix' => 'lt_',
				'width' => 180,
				'height' => 180,
			),
			'small_thumb' => array(
				'prefix' => 'st_',
				'width' => 120,
				'height' => 120,
			),
		),
		
		'feedback_img_width' => 180,
		'feedback_img_height' => 155,
		'feedback' => array(
			'small_thumb' => array(
				'prefix' => 'st_',
				'width'	=> 120,
				'height' => 120
			)
		),
		
		'page_img_width' => 175,
		'page_img_height' => 155,
		'page' => array(
			'small_thumb' => array(
				'prefix' => 'st_',
				'width' => 120,
				'height' => 120
			)
		)			
	);

	function __construct($id = false)
	{
		$this->table = new Table_Images();

        if($id)
        {
	        $select = $this->table->select()
						->from(array('pimg' => $this->table->getName()))
						->where('pimg.pimg_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id	= $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'pimg_id = ' . $this->id);
    }
    
    // type = 'stable';'advert';'marker' (string) : ref_id = stable_id ; advert_id (int)
    public function listItems($type = false, $ref_id = false)
    {
    	if($type && $ref_id)
    	{
	        $select = $this->table->select()
			//			->setIntegrityCheck(false)	// allows joins
						->from(array('pimg' => $this->table->getName()))
			//			->joinLeft(array('lstb' => $this->lang_table->getName()), 'lstb.lstb_pstb_id = pstb.pstb_id')
						->where('pimg.pimg_ref_id = ?', $ref_id)
						->where('pimg.pimg_type = ?', $type)
						->order('pimg.pimg_order');
								
			if ($row = $this->table->fetchAll($select))
			{
			    return $row->toArray();
			}
		}
	}
	
	public function getFirst($type = false, $ref_id = false)
	{
		if($type && $ref_id)
    	{
			$select = $this->table->select()
						->from(array('pimg' => $this->table->getName()))
						->where('pimg.pimg_ref_id = ?', $ref_id)
						->where('pimg.pimg_type = ?', $type)
						->order('pimg.pimg_order')
						->limit(1);
		
			if ($row = $this->table->fetchAll($select))
			{
			    return $row->toArray();
			}
		}
	}
	
	public function deleteItem()
	{
		if(!$this->data)
		{
			return false;
		}
		
		// settings
		$uploads_dir = ROOT_PATH . '/public/uploads/' . $this->data['pimg_type'] . 'images/' . $this->data['pimg_ref_id'] . DS;
		
		// files
		$file_list = array(
			$uploads_dir . $this->data['pimg_name'],											// original image
			$uploads_dir . self::$image_sizes['full_size_prefix'] . $this->data['pimg_name']	// full size image
		);
		
		foreach(self::$image_sizes[$this->data['pimg_type']] as $info)
		{
			$file_list[] = $uploads_dir . $info['prefix'] . $this->data['pimg_name'];			// predefined image sizes
		}
		
		// iterate through files, remove if exists
		foreach($file_list as $file)
		{
			if(file_exists($file))
			{
				@unlink($file);
			}
		}

		// remove from db
 		$result = $this->table->delete(array('pimg_id = ?' => $this->id));

		// remove directory if empty
		if(count($this->listItems($this->data['pimg_type'], $this->data['pimg_ref_id'])) == 0)
		{
			rmdir($uploads_dir);
		}
		
		return $result;
	}

	public function setOrder($order) {
		return $this->table->update(array('pimg_order' => $order), 'pimg_id = ' . $this->id);
	}
	
	public function createImages($src_img_name, $uploads_dir, $ref_id, $ref_type, $banner_type = null) 
	{
		$image = new Zend_Image_Transform($uploads_dir . $src_img_name, new Zend_Image_Driver_Gd);
		unlink($uploads_dir . $src_img_name); // Delete original image
		
		switch($ref_type)
		{
			case 'gallery':
			case 'feedback':
			case 'page':
				$src_img_width = $image->getWidth();
				$src_img_height = $image->getHeight();
				
				$w = self::$image_sizes[$ref_type . '_img_width'];
				$h = self::$image_sizes[$ref_type . '_img_height'];
				
				// Target ratio
				$tr = $w / $h;
				// Source ratio
				$sr = $src_img_width / $src_img_height;
				
				// Save full sized copy (max 1650px wide)
				if($src_img_width > 1650)
					$image->fitToWidth(1650);
				$image->save($uploads_dir . self::$image_sizes['full_size_prefix'] . $src_img_name);
				
				// If horizontal & source ratio larger than 1.35
				if($tr > $sr && $sr > 1.35) // Avg target ratio (advert ratio - 1.3469..., stables ratio - 1.333...)
				{
					// Strict crop
					$image->fitOut($w, $h)
							->center()
							->crop($w, $h)
							->save($uploads_dir . $src_img_name);
		
					foreach(self::$image_sizes[$ref_type] as $size => $info)
					{
						$image->fitOut($info['width'], $info['height'])
								->center()
								->crop($info['width'], $info['height'])
								->save($uploads_dir . $info['prefix'] . $src_img_name);
					}
				}
				else
				{
					$image->fitOut($w, $h)
							->middle()
							->crop($w, $h)
							->save($uploads_dir . $src_img_name);
		
					foreach(self::$image_sizes[$ref_type] as $size => $info) 
					{
						$image->fitOut($info['width'], $info['height'])
								->middle()
								->crop($info['width'], $info['height'])
								->save($uploads_dir . $info['prefix'] . $src_img_name);
					}
				}
			
			break;
			
		case 'banner':
			$src_width = $image->getWidth();
			$src_height = $image->getHeight();
			
			$image->fitToWidth(self::$image_sizes[$banner_type . '_banner_img_width'])
					->save($uploads_dir . $src_img_name);
					
			foreach(self::$image_sizes[$ref_type] as $size => $info) 
			{
				$image->fitOut($info['width'], $info['height'])
						->middle()
						->crop($info['width'], $info['height'])
						->save($uploads_dir . $info['prefix'] . $src_img_name);
			}
			
			break;
		
		default:
			$image->save($uploads_dir . $src_img_name);
			
			break;
		}

		$data['pimg_name'] = $src_img_name;
		$data['pimg_type'] = $ref_type;
		$data['pimg_ref_id'] = $ref_id;

		return $this->save($data);
	}
}