<?php

class Model_OrderItems
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	function __construct( $id = false )
	{
		$this->table = new Table_OrderItems();

		if ($id)
		{
		    $this->id = $id;
		}
	}

    public function save()
    {
		$table=$this->table;
		// Validate fields here 

        $fields = $table->info( Zend_Db_Table_Abstract::COLS );

        foreach ( $this->data as $field => $value ) {
            if ( !in_array( $field, $fields ) ) {
                unset( $this->data[$field] );
            }
        }

        // insert only
		$result = $table->insert( $this->data );
		if ( $result > 0 )
			$this->id = $result;
			
		return $result;
    }
    
    public function listItems($lang = 'lv')
    {
        $select = $this->table->select()
						->setIntegrityCheck( false )	// allows joins
						->from( array( 'poit' => $this->table->getName() ) )
						->join( array( 'pgal' => 'pub_galleries' ), 'pgal.pgal_id = poit.poit_pgal_id' )
						->joinLeft( array( 'lgal' => 'lng_galleries' ), 'lgal.lgal_pgal_id = poit.poit_pgal_id' )
						->joinLeft( array( 'clss1' => 'cls_sizes' ), 'clss1.clss_id = poit.poit_clss_id1', array( 'clss1_width' => 'clss1.clss_width',
																												  'clss1_height' => 'clss1.clss_height' ) )
						->joinLeft( array( 'clss2' => 'cls_sizes' ), 'clss2.clss_id = poit.poit_clss_id2', array( 'clss2_width' => 'clss2.clss_width',
																												  'clss2_height' => 'clss2.clss_height' ) )
						->joinLeft( array( 'clss3' => 'cls_sizes' ), 'clss3.clss_id = poit.poit_clss_id3', array( 'clss3_width' => 'clss3.clss_width',
																												  'clss3_height' => 'clss3.clss_height' ) )
						->joinLeft( array( 'cprt1' => 'cls_product_types' ), 'cprt1.cprt_id = clss1.clss_cprt_id', array( 'cprt1_name' => 'cprt1.cprt_name' ) )
						->joinLeft( array( 'cprt2' => 'cls_product_types' ), 'cprt2.cprt_id = clss2.clss_cprt_id', array( 'cprt2_name' => 'cprt2.cprt_name' ) )
						->joinLeft( array( 'cprt3' => 'cls_product_types' ), 'cprt3.cprt_id = clss3.clss_cprt_id', array( 'cprt3_name' => 'cprt3.cprt_name' ) )
						->where( 'poit.poit_pord_id = ?', $this->id )
						->where( 'lgal.lgal_clsl_code = ?', $lang );
						
		if ( $row = $this->table->fetchAll( $select ) )
		{
		    return $row->toArray();
		}
	}
	
    public function listFactoryItems()
    {
        $select = $this->table->select()
						->setIntegrityCheck( false )	// allows joins
						->from( array( 'poit' => $this->table->getName() ) )
						->join( array( 'pfac' => 'pub_factory' ), 'pfac.pfac_id = poit.poit_pfac_id' )
//						->joinLeft( array( 'pfai1' => 'pub_factory_items' ), 'pfai.pfai_pfac_id = pfac.pfac_id', array() )
						
						->joinLeft( array( 'clss1' => 'cls_sizes' ), 'clss1.clss_id = poit.poit_clss_id1', array( 'clss1_width' => 'clss1.clss_width',
																												  'clss1_height' => 'clss1.clss_height' ) )
						->joinLeft( array( 'clss2' => 'cls_sizes' ), 'clss2.clss_id = poit.poit_clss_id2', array( 'clss2_width' => 'clss2.clss_width',
																												  'clss2_height' => 'clss2.clss_height' ) )
						->joinLeft( array( 'clss3' => 'cls_sizes' ), 'clss3.clss_id = poit.poit_clss_id3', array( 'clss3_width' => 'clss3.clss_width',
																												  'clss3_height' => 'clss3.clss_height' ) )
						->joinLeft( array( 'clss4' => 'cls_sizes' ), 'clss4.clss_id = poit.poit_clss_id4', array( 'clss4_width' => 'clss4.clss_width',
																												  'clss4_height' => 'clss4.clss_height' ) )
						->joinLeft( array( 'cprt1' => 'cls_product_types' ), 'cprt1.cprt_id = clss1.clss_cprt_id', array( 'cprt1_name' => 'cprt1.cprt_name_singular' ) )
						->joinLeft( array( 'cprt2' => 'cls_product_types' ), 'cprt2.cprt_id = clss2.clss_cprt_id', array( 'cprt2_name' => 'cprt2.cprt_name_singular' ) )
						->joinLeft( array( 'cprt3' => 'cls_product_types' ), 'cprt3.cprt_id = clss3.clss_cprt_id', array( 'cprt3_name' => 'cprt3.cprt_name_singular' ) )
						->joinLeft( array( 'cprt4' => 'cls_product_types' ), 'cprt4.cprt_id = clss4.clss_cprt_id', array( 'cprt4_name' => 'cprt4.cprt_name_singular' ) )
						->where( 'poit.poit_pord_id = ?', $this->id );
						
		if ( $row = $this->table->fetchAll( $select ) )
		{
		    return $row->toArray();
		}
	}	
	
	public function deleteItems()
	{
		$select = $this->table->delete( array( 'poit_pord_id = ?' => $this->id ) );
		
		return $select;
	}
}