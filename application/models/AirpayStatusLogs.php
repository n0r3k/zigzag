<?php

class Model_AirpayStatusLogs
{
    protected $table = null;

	function __construct()
	{
		$this->table = new Table_AirpayStatusLogs();
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

        // insert only
		$result= $table->insert($data);
			
		return $result;
    }
    
    public function listItems()
    {
        $select = $this->table->select()
					->from(array('slog' => $this->table->getName()))
					->order('slog.slog_timestamp');
							
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}	
}