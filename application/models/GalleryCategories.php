<?php

class Model_GalleryCategories
{
    protected $table=null;
	protected $id = null;

	public $data = array();

	function __construct($id=false)
	{
		$this->table = new Table_GalleryCategories();

		if ($id)
		{
	        $select = $this->table->select()
						->from(array('ccat' => $this->table->getName()))
						->where('ccat.ccat_id = ?', $id);

			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
                
		if (empty($this->id))
		{
			$result = $table->insert($data);
			if ($result > 0)
				$this->id = $result;
				
			return $result;
		}
		else
			return $table->update($data, 'ccat_id = ' . $this->id);
    }
    
    public function listItems()
    {
        $select = $this->table->select()
						->from(array('ccat' => $this->table->getName()))
						->order(array('ccat.ccat_order ASC'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
		
	public function deleteItem()
	{
		$result = $this->table->delete(array('ccat_id = ?' => $this->id));
		
		return $result;
	}
}