<?php

class Model_Gallery
{
    protected $table 	= null;
	protected $id 		= null;
	protected $lang 	= null;

	protected $table_lang;
	protected $prefix;
	protected $prefix_lang;
	
	public $data = array();

	function __construct($id = false, $lang = 'lv')
	{
		$this->lang 		= $lang;
		$this->table 		= new Table_Galleries();
		$this->table_lang 	= new Table_GalleryLanguages();

		$this->prefix		= $this->table->getPrefix();
		$this->prefix_lang	= $this->table_lang->getPrefix();		

        if($id)
        {
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('pgal' => $this->table->getName()))
						->joinLeft(array('lgal' => $this->table_lang->getName()), 'lgal.lgal_pgal_id = pgal.pgal_id')
						->where('lgal.lgal_clsl_code = ?', $lang)
						->where('lgal.lgal_pgal_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			}

		    $this->id	= $id;
		}
	}
	
	public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, $this->prefix . '_id = ' . $this->id);
    }

	public function getId()
	{
		return $this->id;
	}

    public function updateMinPrice()
    {
    	if ($this->id)
    	{
		    $select = $this->table->select()
		    			->setIntegrityCheck( false )
		    			->from( array( 'xgls' => 'clss_x_pgal' ), array( 'min_price' => 'min(xgls.xgls_price)' ) )
		    			->join( array( 'clss' => 'cls_sizes' ), 'clss.clss_id = xgls.xgls_clss_id', array() )
		    			->where( 'xgls.xgls_pgal_id = ?', $this->id )
		    			->group( 'clss.clss_cprt_id' );
	
			if ($row = $this->table->fetchAll($select))
			{
				$data['pgal_starting_price'] = 0;
	
				foreach ( $row as $cat_price )
				{
					$data['pgal_starting_price'] += $cat_price['min_price'];
				}
	
				return $this->save($data);
			}
		}
		
		return false;
	}

    /* Lists all items for the specified language */
    public function listItems($lang = 'lv')
    {
        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array($this->prefix => $this->table->getName()))
					->joinLeft(array($this->prefix_lang => $this->table_lang->getName()), $this->prefix_lang . '.' . $this->prefix_lang . '_' . $this->prefix . '_id = ' . $this->prefix . '.' . $this->prefix . '_id')
					->where($this->prefix_lang . '.' . $this->prefix_lang . '_clsl_code = ?', $lang)
//					->where($this->prefix . '_is_active = ?', 1)
					->order($this->prefix . '_id DESC');
							
		if ($row = $this->table->fetchAll($select))
		    return $row->toArray();
	}

    public function listActiveItems($category_id = false, $lang = 'lv')
    {
        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array($this->prefix => $this->table->getName()))
					->joinLeft(array($this->prefix_lang => $this->table_lang->getName()), $this->prefix_lang . '.' . $this->prefix_lang . '_' . $this->prefix . '_id = ' . $this->prefix . '.' . $this->prefix . '_id')
					->where($this->prefix_lang . '.' . $this->prefix_lang . '_clsl_code = ?', $lang)
					->where($this->prefix . '_is_active = ?', 1)
					->order($this->prefix . '_id DESC');
							
		if ($category_id)
		{
			$select->where($this->prefix . '_ccat_id = ?', $category_id);
		}
							
		if ($row = $this->table->fetchAll($select))
		    return $row->toArray();
	}	
	
	/* Permanently delete gallery */
	public function deleteItem()
	{	
		$result = $this->table->delete(array($this->prefix . '_id = ?' => $this->id));
		
		return $result;
	}
}