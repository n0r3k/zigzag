<?php

class Model_Cart
{
	protected $items = array();
	protected $prices = array();
	protected $cl_info = array();

	protected $vat = 1.21;
	const EUR_TO_LVL = 0.702804;

	public function __construct()
	{
		$cart = new Zend_Session_Namespace('Cart');
		$this->items 	= &$cart->items;		
		$this->prices 	= &$cart->prices;
		$this->cl_info	= &$cart->cl_info;
	}

	public function addGalleryItem(Model_Gallery $item, Model_Size $size1, Model_Size $size2, Model_Size $size3)
	{
		if (!$item->getId())
		{
			throw new Exception('gallery not found');
		}

		if (!($size1->getId() || $size2->getId() || $size3->getId()))
		{
			return false;
		}

		$add_new_item = true;

		if ($this->items)
		{
			foreach ($this->items as &$i)
			{
				if ( $i['_item']['pgal_id']  == $item->getId()
				&&   $i['_size1']['clss_id'] == $size1->getId()
				&&	 $i['_size2']['clss_id'] == $size2->getId()
				&&	 $i['_size3']['clss_id'] == $size3->getId())
				{
					$i['_qty']++;
					$add_new_item = false;
					break;
				}
			}
		}

		if ($add_new_item)
		{
			$image = new Model_Image();
			$this->items[] = array(
								'_item'  => $item->data,
								'_size1' => $size1->data ? $size1->getDataForCart($item->getId()) : null,
								'_size2' => $size2->data ? $size2->getDataForCart($item->getId()) : null,
								'_size3' => $size3->data ? $size3->getDataForCart($item->getId()) : null,
								'_image' => $image->getFirst('gallery', $item->getId()),
								'_qty'	 => 1
			);
		}

		// calculate prices
		$this->calcPrices();
	}

	public function addFactoryItem(Model_Factory $item)
	{
		if (!$item->getId())
		{
			throw new Exception('constructor not found');
		}

		$add_new_item = true;

		if ($this->items)
		{
			foreach ($this->items as &$i)
			{
				if ( $i['_item']['pfac_id']  == $item->getId())
				{
					$i['_qty']++;
					$add_new_item = false;
					break;
				}
			}
		}

		$m_f_items = new Model_FactoryItems($item->getId());
		$f_items = $m_f_items->listItems();

		if ( !empty( $f_items ) )
		{
			foreach ( $f_items as $f_item )
			{	
				$m_size = new Model_Size($f_item['pfai_clss_id']);
				$m_pr_type = new Model_ProductTypes($f_item['pfai_cprt_id']);
				$sizes[] = $m_size->data + $m_pr_type->data;
			}	
		}		

		if ($add_new_item)
		{
			$this->items[] = array(
								'_item'  => $item->data,
								'_size1' => $sizes[0],
								'_size2' => $sizes[1],
								'_size3' => $sizes[2],
								'_size4' => $sizes[3],
								'_qty'	 => 1
			);
		}

		// calculate prices
		$this->calcPrices();
	}
	
	protected function calcPrices()
	{
		// reset prices
		$this->prices = array();

		if ($this->items)
		{
			foreach ($this->items as &$item)
			{	
				if (isset($item['_item']['pfac_total']))
				{
					$item['_total'] = $item['_item']['pfac_total'];
				}
				else
				{
					$item['_total'] = number_format(
										($item['_size1']['xgls_price'] ? $item['_size1']['xgls_price'] : 0) +
										($item['_size2']['xgls_price'] ? $item['_size2']['xgls_price'] : 0) +
										($item['_size3']['xgls_price'] ? $item['_size3']['xgls_price'] : 0), 2);
				}

				$item['_total_lvl']	 	 = number_format($item['_total'] * self::EUR_TO_LVL, 2);
			
				// ---
				$item['_item_total']     = $item['_total'] * $item['_qty'];
				// update prices without number format to avoid errors after 1,000
				$this->prices['_total'] += $item['_item_total'];
				$this->prices['_qty']	+= $item['_qty'];
				
				$item['_item_total_lvl'] = number_format($item['_item_total'] * self::EUR_TO_LVL, 2);
				$item['_item_total'] 	 = number_format($item['_item_total'], 2);
			}
		}
		else
		{
			$this->prices['_qty'] = 0;
		}

		$this->prices['_subtotal'] 	= $this->prices['_total'] / $this->vat;
		$this->prices['_vat'] 		= number_format($this->prices['_total'] - $this->prices['_subtotal'], 2);
		$this->prices['_total'] 	= number_format($this->prices['_total'], 2);
	
		$this->prices['_subtotal'] = number_format($this->prices['_subtotal'], 2);
	}
		
	public function delGalleryItem($cart_id)
	{
		unset($this->items[$cart_id]);	

		// recalculate prices
		$this->calcPrices();

		return array(
			'_cart_subtotal' 	=> $this->prices['_subtotal'],
			'_cart_vat' 		=> $this->prices['_vat'],
			'_cart_total' 		=> $this->prices['_total'],
			'_cart_qty'			=> $this->prices['_qty']
		); 
	}	

	public function setItemAmount($cart_id, $amount)
	{
		$this->items[$cart_id]['_qty'] = $amount;
		
		$this->calcPrices();
				
		return array(
			'_item_total' 		=> $this->items[$cart_id]['_item_total'],
			'_item_total_lvl' 	=> $this->items[$cart_id]['_item_total_lvl'],
			'_cart_subtotal' 	=> $this->prices['_subtotal'],
			'_cart_vat' 		=> $this->prices['_vat'],
			'_cart_total' 		=> $this->prices['_total'],
			'_cart_qty'			=> $this->prices['_qty']
		); 
	}

	public function clear()
	{
		$this->items = array();
		$this->prices = array();
		$this->cl_info = array();
	}

	public function getItems()
	{
		$this->calcPrices();
		return $this->items;
	}
	
	public function getPrices()
	{
		$this->calcPrices();
		return $this->prices;
	}
}