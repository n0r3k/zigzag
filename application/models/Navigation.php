<?php

class Model_Navigation
{
    protected $table=null;
	protected $id = null;
	protected $lang = null;

	protected $table_lang;

	public $data = array();

	function __construct($code = false, $lang = 'lv')
	{
		$this->lang 		= $lang;
		$this->table 		= new Table_Navigation();
		$this->table_lang 	= new Table_NavigationLanguages();

        if($code)
        {
			$select = $this->table->select()
						->from(array('pnav' => $this->table->getName()))
						->where('pnav.pnav_code = ?', $code);
    
			if($row = $this->table->fetchRow($select))
			{
			    $this->id = $row['pnav_id'];
			}
        
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('pnav' => $this->table->getName()))
						->joinLeft(array('lnav' => $this->table_lang->getName()), 'lnav.lnav_pnav_id = pnav.pnav_id')
						->where('lnav.lnav_clsl_code = ?', $this->lang)
						->where('pnav.pnav_code = ?', $code);
		
			if($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $this->data['pnav_id'];
			}
		}
	}
	
	public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'pnav_id = ' . $this->id);
    }

	public function getId()
	{
		return $this->id;
	}
}