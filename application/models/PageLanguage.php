<?php

class Model_PageLanguage
{
    protected $table=null;

	function __construct()
	{
		$this->table = new Table_PageLanguages();
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

        // insert only
		$result = $table->insert($data);

		// clean history
/* 		$this->cleanHistory($data['lpag_ppag_id']); */
			
		return $result;
    }
	
	public function cleanHistory($page_id)
	{	
		$language = new Model_Language();
		$lang_list = $language->listItems();
		$safe_ids = array();
		
		foreach($lang_list as $lang)
		{
			$select = $this->table->select()
						->from(array('lpag' => 'lng_pages'), array('lpag_id'))
						->where('lpag.lpag_ppag_id = ?', $page_id)
						->where('lpag.lpag_clsl_code = ?', $lang['clsl_code'])
						->order('lpag.lpag_timestamp DESC')
						->limit(10);

			$rows = $this->table->fetchAll($select);
			
			if(!empty($rows))
			{
				foreach($rows as $row)
				{
					$safe_ids[] = $row['lpag_id'];
				}
	
				$condition = array(
					'lpag_ppag_id = ?' => $page_id,
					'lpag_clsl_code = ?' => $lang['clsl_code'],
					'lpag_ppag_id NOT IN(?)' => $safe_ids
				);

				$this->table->delete($condition);
			}
		}
	}
}