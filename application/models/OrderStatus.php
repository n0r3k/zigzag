<?php

class Model_OrderStatus
{
	const ORDER_STATUS_CODE_NEW = 'new';
	const ORDER_STATUS_CODE_APPROVED = 'approved';
	const ORDER_STATUS_CODE_COMPLETED = 'completed';
	const ORDER_STATUS_CODE_CANCELED = 'canceled';
	
    protected $table = null;

	function __construct()
	{
		$this->table = new Table_OrderStatus();
	}
    
    public function listItems()
    {
        $select = $this->table->select()
						->from(array('cost' => $this->table->getName()))
						->order(array('cost.cost_order ASC'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function getIdByCode($code)
	{
		$this->table = new Table_OrderStatus();

        $select = $this->table->select()
						->from(array('cost' => $this->table->getName()), array('cost_id' => 'cost.cost_id'))
						->where('cost.cost_code = ?', $code);
		
		if ($row = $this->table->fetchRow($select)->toArray())
		{
		    return $row['cost_id'];
		}		
	}
}