<?php

class Model_ProductTypes
{
    protected $table = null;
    protected $id = null;
    
    public $data = array();

	function __construct( $id = false )
	{
		$this->table = new Table_ProductTypes();
	
		if ( $id )
		{
	        $select = $this->table->select()
						->from(array('cprt' => $this->table->getName()))
						->where('cprt.cprt_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id	= $id;
			}			
		}
	}
    
    public function listItems()
    {
        $select = $this->table->select()
						->from(array('cprt' => $this->table->getName()))
						->order('cprt.cprt_order ASC');
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}		

	public function getIdByCode( $code )
	{
        $select = $this->table->select()
						->from(array('cprt' => $this->table->getName()), array('cprt.cprt_id'))
						->where('cprt.cprt_code = ?', $code);
		
		if ($row = $this->table->fetchRow($select))
		{
			$row = $row->toArray();
		    return $row['cprt_id'];
		}		
	}
}