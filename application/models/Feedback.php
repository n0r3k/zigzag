<?php

class Model_Feedback
{
    protected $table 	= null;
	protected $id 		= null;
	protected $lang 	= null;

	protected $table_lang;
	protected $prefix;
	protected $prefix_lang;
	
	public $data = array();

	function __construct($id = false, $lang = 'lv')
	{
		$this->lang 		= $lang;
		$this->table 		= new Table_Feedback();
		$this->table_lang 	= new Table_FeedbackLanguages();

		$this->prefix		= $this->table->getPrefix();
		$this->prefix_lang	= $this->table_lang->getPrefix();		

        if($id)
        {
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('pfbk' => $this->table->getName()))
						->joinLeft(array('lfbk' => $this->table_lang->getName()), 'lfbk.lfbk_pfbk_id = pfbk.pfbk_id')
						->where('lfbk.lfbk_clsl_code = ?', $lang)
						->where('lfbk.lfbk_pfbk_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id	= $id;
			}
		}
	}
	
	public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, $this->prefix . '_id = ' . $this->id);
    }

	public function getId()
	{
		return $this->id;
	}

    /* Lists all items for the specified language */
    public function listItems($lang = 'lv')
    {
        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array($this->prefix => $this->table->getName()))
					->joinLeft(array($this->prefix_lang => $this->table_lang->getName()), $this->prefix_lang . '.' . $this->prefix_lang . '_' . $this->prefix . '_id = ' . $this->prefix . '.' . $this->prefix . '_id')
					->where($this->prefix_lang . '.' . $this->prefix_lang . '_clsl_code = ?', $lang)
					->order($this->prefix . '_id DESC');
							
		if ($row = $this->table->fetchAll($select))
		    return $row->toArray();
	}
		
	/* Permanently delete stable */
	public function deleteItem()
	{	
		$result = $this->table->delete(array($this->prefix . '_id = ?' => $this->id));
		
		return $result;
	}
}