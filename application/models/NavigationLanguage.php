<?php

class Model_NavigationLanguage
{
    protected $table = null;

	function __construct()
	{
		$this->table = new Table_NavigationLanguages();
	}

	public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($data['lnav_id']))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'lnav_id = ' . $data['lnav_id']);
    }
}