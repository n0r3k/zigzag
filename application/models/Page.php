<?php

class Model_Page
{
    protected $table=null;
	protected $id = null;
	protected $lang = null;

	protected $table_lang;

	public $data = array();

	// code = 'contacts'
	function __construct($code = false, $lang = 'lv', $version_id = false)
	{
		$this->lang 		= $lang;
		$this->table 		= new Table_Pages();
		$this->table_lang 	= new Table_PageLanguages();

        if($code)
        {
			$select = $this->table->select()
						->from(array('ppag' => $this->table->getName()))
						->where('ppag.ppag_code = ?', $code);
    
			if($row = $this->table->fetchRow($select))
			{
			    $this->id = $row['ppag_id'];
			}
        
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('ppag' => $this->table->getName()))
						->joinLeft(array('lpag' => $this->table_lang->getName()), 'lpag.lpag_ppag_id = ppag.ppag_id')
						->where('lpag.lpag_clsl_code = ?', $this->lang)
						->where('ppag.ppag_code = ?', $code)
						->order('lpag.lpag_timestamp DESC')
						->limit(1);
	
			if($version_id)
			{
				$select->where('lpag.lpag_id = ?', $version_id);
			}
	
			if($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $this->data['ppag_id'];
			}
		}
	}
	
	public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'ppag_id = ' . $this->id);
    }

	public function getId()
	{
		return $this->id;
	}

    public function fetchHistory()
    {
        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array('ppag' => $this->table->getName()))
					->joinLeft(array('lpag' => $this->table_lang->getName()), 'lpag.lpag_ppag_id = ppag.ppag_id')
					->joinLeft(array('sysu' => 'sys_users'), 'sysu.sysu_id = lpag.lpag_sysu_id', array('sysu_name', 'sysu_surname'))
						->where('lpag.lpag_clsl_code = ?', $this->lang)
						->where('ppag.ppag_id = ?', $this->id)
					->order('lpag.lpag_timestamp DESC')
					->limit(10);
														
		if($row = $this->table->fetchAll($select))
		{
			$row = $row->toArray();

			foreach($row as &$v)
			{
				$v['lpag_timestamp'] = $this->formatDate($v['lpag_timestamp']);
			}
			
		    return $row;
		}
	}
	
	public function formatDate($in_out_date)
	{
		if (!$in_out_date)
		{
			return false;
		}
		
		list($date, $time) = explode(' ', $in_out_date);
		list($hour, $minute, $second) = explode(':', $time);
		list($year, $month, $day) = explode('-', $date);
		
		if(date('dmY') == $day . $month . $year)
		{
			$in_out_date = 'Šodien';
		}
		elseif(date('dmY', strtotime('-1 day')) == $day . $month . $year)
		{
			$in_out_date = 'Vakar';
		}
		elseif(date('Y') == $year)
		{
			$in_out_date = date('d M', mktime($hour, $minute, $second, $month, $day, $year));
		}
		else
		{
			$in_out_date = date('d M, Y', mktime($hour, $minute, $second, $month, $day, $year));
		}
		
		if(date('Y') == $year)
		{
			$in_out_date .= ', ' . $hour . ':' . $minute;
		}
		
		return $in_out_date;
	}
	
	/* FRONT */
	public function getListByGroup($group)
	{
		$subselect = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array($this->table_lang->getName()), array('lpag_ppag_id' => 'lpag_ppag_id', 'lpag_timestamp' => 'max(lpag_timestamp)'))
					->where('lpag_clsl_code = ?', $this->lang)	
					->group('lpag_ppag_id');

        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array('ppag' => $this->table->getName()))
					->join(array('lpag' => $this->table_lang->getName()), 'lpag.lpag_ppag_id = ppag.ppag_id')
					->join(array('lpag2' => $subselect), 'lpag2.lpag_ppag_id = lpag.lpag_ppag_id AND lpag2.lpag_timestamp = lpag.lpag_timestamp', array())
					->where('ppag.ppag_group = ?', $group)
					->order('ppag.ppag_order ASC');	

		if($row = $this->table->fetchAll($select))
		{
			return $row->toArray();
		}
	}
}