<?php

class Model_Language
{
    protected $table=null;
	protected $id = null;

	public $data = array();

	function __construct($id=false)
	{
		$this->table = new Table_Languages();

		if ($id)
		{
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('clsl' => $this->table->getName()))
//						->joinLeft(array('t' => 'payment_type'), 'a.payment_type_id = t.id', array("is_name", "is_credit", "payment_type_name" => "name"))
						->where('clsl.clsl_id = ?', $id);

			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
                
		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'clsl_id='.$this->id);
    }
    
    public function listItems()
    {
        $select = $this->table->select()
						->from(array('clsl' => $this->table->getName()))
						->order(array('clsl.clsl_is_default DESC', 'clsl.clsl_is_active DESC', 'clsl.clsl_order ASC', 'clsl.clsl_code'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function listActive()
    {
        $select = $this->table->select()
						->from(array('clsl' => $this->table->getName()))
						->where('clsl.clsl_is_active = 1')
						->order(array('clsl.clsl_is_default DESC', 'clsl.clsl_order ASC', 'clsl.clsl_code'));
		
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function deleteItem()
	{
		if($this->id == 1)
		{
			return false;	
		}
		
		$result = $this->table->delete(array('clsl_id = ?' => $this->id));
		
		return $result;
	}
	
	public function getIdByCode($code)
	{
		if(!$code)
			return;
			
		$select = $this->table->select()
					->from(array('clsl' => $this->table->getName()))
					->where('clsl.clsl_code = ?', $code);
					
		if($row = $this->table->fetchRow($select))
		{
			return $row->clsl_id;
		}
	}
}