<?php

class Model_GalleryLanguage
{
    protected $table=null;
	protected $id = null;

	public $data_all = array();
	
	public $data = array();

	function __construct($id = false, $lang = 'lv')
	{
		$this->table = new Table_GalleryLanguages();

        if($id)
        {
	        $select = $this->table->select()
						->from(array('lgal' => $this->table->getName()))
						->where('lgal.lgal_clsl_code = ?', $lang)
						->where('lgal.lgal_pgal_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			}

	        $select = $this->table->select()
						->from(array('lgal' => $this->table->getName()))
						->where('lgal.lgal_pgal_id = ?', $id);
	
			if ($row = $this->table->fetchAll($select))
			{
			    $this->data_all = $row->toArray();
			}

		    $this->id = $id;
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

        $insert = true;

        if(!empty($this->data_all))
        {
	        foreach($this->data_all as $item)
	        {
		        if($item['lgal_clsl_code'] == $data['lgal_clsl_code'])
		        {
			        $insert = false;
			        break;
		        }
	        }
        }
        
		if ($insert)
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
		{
			$where = array(
				'lgal_pgal_id = ?' => $this->id,
				'lgal_clsl_code = ?' => $data['lgal_clsl_code']);

			return $table->update($data, $where);
		}
    }

	public function deleteItem()
	{	
		$result = $this->table->delete(array('lgal_pgal_id = ?' => $this->id));
		
		return $result;
	}
}