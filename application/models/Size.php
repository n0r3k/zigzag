<?php

class Model_Size
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	function __construct($id = false)
	{
		$this->table = new Table_Sizes();

        if($id)
        {
	        $select = $this->table->select()
						->from(array('clss' => $this->table->getName()))
						->where('clss.clss_id = ?', $id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id	= $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'clss_id = ' . $this->id);
    }
    
    public function listItems( $type = false )
    {
        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array('clss' => $this->table->getName()))
					->joinLeft(array('cprt' => 'cls_product_types'), 'cprt.cprt_id = clss.clss_cprt_id');

		if ( $type )
		{
			$select->where('cprt.cprt_code = ?', $type)
                   ->order('clss.clss_order ASC');
		}
		else
		{
			$select->order(array('clss.clss_cprt_id ASC', 'clss.clss_order ASC'));
		}
							
		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}
	}
	
	public function deleteItem()
	{
		if(!$this->data)
		{
			return false;
		}
	
		// remove from db
 		$result = $this->table->delete(array('clss_id = ?' => $this->id));

		return $result;
	}
	
	public function getDataForCart($gallery_id)
	{
		if (!$this->id || !$gallery_id)
		{
			return false;
		}

        $select = $this->table->select()
					->setIntegrityCheck(false)	// allows joins
					->from(array('clss' => $this->table->getName()))
					->join(array('xgls' => 'clss_x_pgal'), 'xgls.xgls_clss_id = clss.clss_id and xgls.xgls_pgal_id = ' . $gallery_id)
					->join(array('cprt' => 'cls_product_types'), 'cprt.cprt_id = clss.clss_cprt_id')
					->where('clss.clss_id = ?', $this->id);

		if ($row = $this->table->fetchRow($select))
		{
			return $row->toArray();
		}
	}
	
	public function getId()
	{
		return $this->id;
	}
}