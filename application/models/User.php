<?php

class Model_User
{
    protected $table=null;
	protected $id = null;

	public $data = array();

	function __construct($id=false)
	{
		$this->table=new Table_Users();

		if ($id)
		{
	        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('sysu' => $this->table->getName()), array('sysu_id', 'sysu_username', 'sysu_name', 'sysu_surname', 'sysu_email', 'sysu_is_admin', 'sysu_is_active'))
//						->joinLeft(array('t' => 'payment_type'), 'a.payment_type_id = t.id', array("is_name", "is_credit", "payment_type_name" => "name"))
						->where('sysu.sysu_id = ?', $id);

			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id = $id;
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
			
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        
        if (isset($data['sysu_password']))
        {
	        $data['sysu_password'] = '*' . sha1(sha1($data['sysu_password'], true));
        }
        
		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, "sysu_id=".$this->id);
    }
    
    public function listUsers()
    {
        $select = $this->table->select()
						->setIntegrityCheck(false)	// allows joins
						->from(array('sysu' => $this->table->getName()))
//						->joinLeft(array('t' => 'payment_type'), 'a.payment_type_id = t.id', array("payment_type_name" => "name", "icon"))
//						->where('a.user_id = ?', $_SESSION["user_id"])
						->order(array('sysu.sysu_is_admin DESC', 'sysu.sysu_username'));
		
		if ($row = $this->table->fetchAll($select))
		    return $row->toArray();
	}
	
	public function deleteUser()
	{
		if($this->id == 1)
		{
			return false;	
		}
		
		$result = $this->table->delete(array('sysu_id = ?' => $this->id));
		
		return $result;
	}
}