<?php

class Model_SizePrices
{
    protected $table = null;
	protected $id = null;

	public $data = array();

	public function __construct($gallery_id = false, $size_id = false)
	{
		$this->table = new Table_SizePrices();

        if($gallery_id && $size_id)
        {
	        $select = $this->table->select()
						->from(array('xgls' => $this->table->getName()))
						->where('xgls.xgls_pgal_id = ?', $gallery_id)
						->where('xgls.xgls_clss_id = ?', $size_id);
	
			if ($row = $this->table->fetchRow($select))
			{
			    $this->data = $row->toArray();
			    $this->id	= $this->data['xgls_id'];
			}
		}
	}

    public function save($data)
    {
		$table=$this->table;
		// Validate fields here 
/* 			print_r($data); */
        $fields = $table->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }

/*         print_r($fields); */

		if (empty($this->id))
		{
			$result= $table->insert($data);
			if ($result>0)
				$this->id=$result;
				
			return $result;
		}
		else
			return $table->update($data, 'xgls_id = ' . $this->id);
    }

    public function listItemsByGalleryId($id, $size_type = false, $active = false)
    {
        $select = $this->table->select()
        			->setIntegrityCheck(false)	// allows joins
        			->from(array('clss' => 'cls_sizes'))
        			->join(array('cprt' => 'cls_product_types'), 'cprt.cprt_id = clss.clss_cprt_id')
					->order(array('clss.clss_order ASC'));

		if ($id)
		{
			$select->joinLeft(array('xgls' => $this->table->getName()), 'xgls.xgls_clss_id = clss.clss_id AND xgls.xgls_pgal_id = ' . $id);
		}
					
		if ($size_type)
		{
			$select->where('cprt.cprt_code = ?', $size_type);
		}

		if ($active)
		{
			$select->where('xgls.xgls_pgal_id = ?', $id);
		}

		if ($row = $this->table->fetchAll($select))
		{
		    return $row->toArray();
		}	    
    }
    
    public function deleteItem($data = array(), $del_all_prices = false)
	{	
		if($del_all_prices)
		{
			$result = $this->table->delete(array('xgls_pgal_id = ?' => $data['xgls_pgal_id']));
		}
		else
		{
			$result = $this->table->delete(array('xgls_pgal_id = ?' => $data['xgls_pgal_id'], 'xgls_clss_id = ?' => $data['xgls_clss_id']));
		}
		return $result;
	}
}