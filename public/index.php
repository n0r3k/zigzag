<?php

// This is set up in application.ini config
/*
error_reporting (E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
*/

$gotourl = 'http://www.zigzagfactory.com/';

switch (substr($_SERVER['HTTP_HOST'], -3)) {
    case '.ru':
        $gotourl .= 'ru';
	header('Location: ' . $gotourl);
        break;
}

defined('DS')
	|| define('DS', DIRECTORY_SEPARATOR);

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . DS . '..' . DS . 'application'));

defined('ROOT_PATH')
    || define('ROOT_PATH', realpath(dirname(__FILE__) . DS . '..' . DS));

// Define application environment
defined('APPLICATION_ENV')
	|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
	
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(ROOT_PATH . '/library'),
    realpath(APPLICATION_PATH),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';  

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV, 
    APPLICATION_PATH . '/configs/application.ini'
);


$application->bootstrap()
            ->run();