/* jshint multistr: true */
/* global console, gallery_open_id, window, Constructor, Overlay, Cache,
    jQuery, Preview, alert */

(function ($, undefined) {
  $(function () {
    var $pageWrapper = $('div.page-wrapper');

    // Language
    var $lang = $('html').attr('lang');

    // Masonry
    var $masonryContainer = $pageWrapper.find('div.masonry');

    if ($masonryContainer[0]) {
      var gutter = parseInt($masonryContainer.attr('data-gutter'), 10) || 10;

      $masonryContainer.masonry({
        columnWidth: 190,
        gutter: gutter,
        itemSelector: '.item'
      });
    }

    // 
    // LANDING PAGE
    //
    var top_slider = $('.top-slider').bxSlider({
	  auto: true, 
      controls: false,
      pager: false,
      speed: 1000
    });	

    // atsauksmes
    var ats_slider = $('.atsauksmes-slider').bxSlider({
	  auto : true,
	  speed: 1500,
      controls: false,
      pager: false,
      slideWidth: 260,
      minSlides: 3,
      maxSlides: 3,
      moveSlides: 1,
      slideMargin: 15
    });

    $('.atsauksmes-nav.next').on('click', function(e) {
      e.preventDefault();
      ats_slider.goToNextSlide();
    });

    $('.atsauksmes-nav.prev').on('click', function(e) {
      e.preventDefault();
      ats_slider.goToPrevSlide();
    });

    //
    // INDEX PAGE
    //

    // slider
    var slider = $('.slider-content-inner').bxSlider({
      speed: 700,
      auto: true,
      autoHover: true,
      pagerCustom: '.slider-bullets'
    });

    $('.slider-nav.next').on('click', function(e) {
      e.preventDefault();
      slider.goToNextSlide();
    });

    $('.slider-nav.prev').on('click', function(e) {
      e.preventDefault();
      slider.goToPrevSlide();
    });

    // gallery
    var gal_slider = $('.gallery-slider').bxSlider({
      controls: false,
      pager: false,
      slideWidth: 130,
      minSlides: 3,
      maxSlides: 3,
      moveSlides: 1,
      slideMargin: 16
    });

    $('.gallery-nav.next').on('click', function(e) {
      e.preventDefault();
      gal_slider.goToNextSlide();
    });

    $('.gallery-nav.prev').on('click', function(e) {
      e.preventDefault();
      gal_slider.goToPrevSlide();
    });


    //
    // OVERLAY
    //
    $pageWrapper
      .find('div.galerija')
      .on('click', 'article.item', function () {

        var cache = Cache.getInstance(),
            galleryID = $(this).attr('data-item-id'),
            galleryOverlay;

        if (cache.get(galleryID)) {
          galleryOverlay = new Overlay(cache.get(galleryID));
          galleryOverlay.renderTemplateAfter($pageWrapper);
          return;
        }

        $.ajax({
          url: '/ajax/getgallerydetailsbyid',
          type: 'GET',
          data: { id: galleryID, language : $lang },
          success: function (data) {
            var dataObj = JSON.parse(data);

            cache.set(dataObj.pgal_id, dataObj);

            galleryOverlay = new Overlay(dataObj);
            galleryOverlay.renderTemplateAfter($pageWrapper);
          },
          error: function (error) {
            return;
          }
        });
      });

     // simulate click when id passed
     try {
        $('article.item[data-item-id="' + gallery_open_id + '"]').click();
     } catch (err) {
       // do nothing
     }


    //
    // FACTORY / CONSTRUCTOR
    //
    var $factoryContainer = $pageWrapper.find('div.factory-container'),
        $themeSwitchers = $pageWrapper.find('div.theme-switcher > a'),
        $factory = $factoryContainer.find('div.factory'),
        $buyContainer = $pageWrapper.find('div.buy'),
        $pricingContainer = $buyContainer.find('div.pricing'),
        $buyButtons = $buyContainer.find('div.buttons');

    $themeSwitchers.on('click', function () {
      var $this = $(this),
          theme = $(this).attr('data-theme');

      $factoryContainer
        .removeClass('theme-bright theme-dark')
        .addClass('theme-' + theme);

      $themeSwitchers.removeClass('active');
      $this.addClass('active');

      return false;
    });

    $factory.on('click', 'div.trigger', function () {
      var type = $(this).attr('data-constructor-type');

      $.ajax({
        url: '/ajax/openfactorybytype',
        type: 'GET',
        // padodam tipu (duvet - sega, pillow - spilvenam, sheet - palagam)
        data: { type: type },
        success: function (data) {
          var dataObj = JSON.parse(data);
          dataObj.type = type;

          var constructor = new Constructor(dataObj);
          constructor.renderTemplateAfter($pageWrapper);
        },
        error: function (error) {
          return;
        }
      });
    });

    $buyButtons.on('click', 'a.order', function (e) {
        e.preventDefault();

        $.ajax({
          url: '/ajax/addfactoryitemtocart',
          type: 'GET',
//          data: {},
          success: function (data) {
            var dataObj = JSON.parse(data);
            $('#cart_qty').html(dataObj._qty);
            window.location.replace('/' + $lang + '/grozs');          
          },
          error: function (error) {
            return;
          }
        });
    });

    //
    // PREVIEWS
    //
    $.ajax({
      url: '/ajax/loadpreviewsfromsession',
      type: 'POST',
      data: { types: [
          'spilvendrana-1', 'spilvendrana-2', 'palags', 'virspalags'
        ]
      },
      success: function (data) {
        var dataObj = JSON.parse(data),
            previewData = {},
            preview,
            layout;

        var $containers = {
          'virspalags': $factory.find('div.trigger.virspalags'),
          'palags': $factory.find('div.trigger.palags'),
          'spilvendrana-1': $factory.find('div.trigger.spilvendrana-1'),
          'spilvendrana-2': $factory.find('div.trigger.spilvendrana-2')
        };

        var $prices = {
          'virspalags': $pricingContainer.find('span.price-virspalags'),
          'palags': $pricingContainer.find('span.price-palags'),
          'spilvendrana': $pricingContainer.find('span.price-spilvendrana'),
          'total': $pricingContainer.find('div.total-price span.price')
        };

        var total_price = 0;
        var palags_price = 0;
        var virspalags_price = 0;
        var spilvendrana_price = 0;

        $.each(dataObj, function (i, item) {
          if (item) {
            if (i !== 'palags') {
              if (!item.layout || !item.layout.size) {
                return;
              }

              previewData = {
                width: item.size.width,
                height: item.size.height,
                colors: item.layout
              };

              previewData.containerWidth = $containers[i].width();
              previewData.containerHeight = $containers[i].height();

              preview = new Preview(previewData);
              $containers[i]
                .prepend(preview.generateTableLayout())
                .addClass('has-preview');

              if (i === 'virspalags') {
                $containers[i].height($containers[i].find('table').height());
              }

              // prices
              var tmp_price =
                Number(item.size.base_price) + Number(item.design_price);

              if (i === 'virspalags') {
                virspalags_price += tmp_price;
              } else {
                spilvendrana_price += tmp_price;
              }

              total_price += tmp_price;
              // </- prices
            } else {
              previewData = {
                bottomColor: item.bottom_color
              };

              preview = new Preview(previewData);
              preview.setBackgroundColor($containers[i]);

              palags_price += Number(item.size.base_price);
              total_price += palags_price;
            }
          }
        });

        $prices['spilvendrana'].html(spilvendrana_price.toFixed(2) + ' &euro;');
        $prices['virspalags'].html(virspalags_price.toFixed(2) + ' &euro;');
        $prices['palags'].html(palags_price.toFixed(2) + ' &euro;');
        $prices['total'].html(total_price.toFixed(2) + ' &euro;');
      },
      error: function (err) {
        // console.log(err);
      }
    });

    //
    // CART ITEMS
    //
    var $cartItems = $pageWrapper.find('div.cart-items');

    $cartItems
      .find('td.amount')
      .on('click', function (e) {
      var tr = $(this).closest('tr'),
          galleryID = tr.attr('data-item-id'),
          $t = $(e.target),
          $amount = $(this).find('input'),
          oldAmount = $amount.val();

        if ($t.hasClass('incr')) {
          $amount.val(parseInt($amount.val(), 10) + 1);
        } else if ($t.hasClass('decr') && parseInt($amount.val(), 10) > 1) {
          $amount.val(parseInt($amount.val(), 10) - 1);
        }

        $.ajax({
          url: '/ajax/setitemamount',
          type: 'GET',
          data: { id: galleryID, amount: $amount.val() },
          success: function (data) {
            var dataObj = JSON.parse(data);

            tr.find('td.total-price p.eur')
              .html(dataObj['_item_total'] + ' €');
            tr.find('td.total-price p.lat')
              .html(dataObj['_item_total_lvl'] + ' Ls');

            $('#subtotal').html(dataObj['_cart_subtotal'] + ' €');
            $('#vat').html(dataObj['_cart_vat'] + ' €');
            $('#total').html(dataObj['_cart_total'] + ' €');
            $('#cart_qty').html(dataObj['_cart_qty']);
          },
          error: function () {
            $amount.val(oldAmount);
          }
        });

        return false;
      })
      .end()
      .find('a.remove-item')
      .on('click', function () {
        var tr = $(this).closest('tr'),
            galleryID = tr.attr('data-item-id');

        $.ajax({
          url: '/ajax/delgalleryitemfromcart',
          type: 'GET',
          data: { id: galleryID },
          success: function (data) {
            var dataObj = JSON.parse(data);

            $('#subtotal').html(dataObj['_cart_subtotal'] + ' €');
            $('#vat').html(dataObj['_cart_vat'] + ' €');
            $('#total').html(dataObj['_cart_total'] + ' €');
            $('#cart_qty').html(dataObj['_cart_qty']);

            tr.fadeOut(250, function () {
              tr.remove();
            });
          },
          error: function (error) {
            return;
          }
        });
      })
      .end()
      .find('input.item-amount')
      .on('keydown', function (event) {
        return isNumber(event, this);
      });


    //
    // PAYMENTS
    //
    var $paymentForm = $('div.payment');

    $paymentForm
      .find('a.card, a.paypal')
      .on('click', function (event) {
        event.preventDefault();
        $('form').submit();
      });


    //
    // ORDER DATA
    //
    var csBox = {},
        $orderData = $pageWrapper.find('div.order-data'),
        $specificFieldset =
          $orderData.find('fieldset.delivery-specific-details'),
        $deliveryNameMatchesBilling = $('#deliveryNameMatchesBilling');

    csBox.$container = $orderData.find('div.selectbox');
    csBox.$list = csBox.$container.find('div.selectbox-list');
    csBox.$el = csBox.$container.find('a.selectbox-element');
    csBox.$value = csBox.$container.find('input.selectbox-value');

    if ($deliveryNameMatchesBilling.is(':checked')) {
      $specificFieldset.hide();
    } else {
      $specificFieldset.show();
    }

    $deliveryNameMatchesBilling.on('click', function () {
      if ($(this).is(':checked')) {
        $specificFieldset.fadeOut(250);
      } else {
        $specificFieldset.fadeIn(250);
      }
    });

    csBox.$container.on('click', function (e) {
      if (e.target.className === "selectbox-element") {
        csBox.$list.toggle();
      } else if (e.target.className === "selectbox-list-item") {
        var $item = $(e.target);

        csBox.$value.val($item.attr('data-value'));
        csBox.$el.html($item.html());
        csBox.$list.hide();
      }

      return false;
    });

    function isValidFields() {
      var is_valid = true;
      // define mandatory fields
      var mandatory_fields = ['billingFirstName', 'billingLastName',
        'billingEmail', 'billingPhone', 'deliveryName', 'deliveryPhone',
        'deliveryCountry', 'deliveryCity', 'deliveryZip', 'deliveryStreet'];
      // mandatory fields with options
      var other_fields = [ 'deliveryName', 'deliveryPhone' ];

      // hide all errors
      $('.error').hide();

      // check fields
      for (var i = 0; i < mandatory_fields.length; i++) {
        if ($('#' + mandatory_fields[i]).val() === '') {
          var delivery_matches =
            $('#deliveryNameMatchesBilling').prop('checked');

          if (($.inArray(mandatory_fields[i], other_fields) !== -1 &&
              delivery_matches === false) ||
              $.inArray(mandatory_fields[i], other_fields) === -1) {

            is_valid = false;
            $('#error_' + mandatory_fields[i]).show();
          }
        }
      }

      return is_valid;
    }

    $orderData
      .find('div.links')
      .find('a.confirm')
      .on('click', function(e) {
        e.preventDefault();

        if(isValidFields()) {
          $('form').submit();
        }
      });

  //
  // GALLERY DATA
  //
  var $subMenu = $pageWrapper.find('div.sub-menu');

  $subMenu
      .find('a')
      .on('click', function (e) {
        e.preventDefault();

        var categoryID = $(this).attr('data-item-id');
        $subMenu
          .find('a.active')
          .removeClass('active');

            $(this).addClass('active');

        $.ajax({
          url: '/ajax/loadgalleryitems',
          type: 'GET',
          data: { id: categoryID, language: $lang },
          success: function (data) {
            var dataObj = JSON.parse(data);

            var html = '';
            $.each(dataObj, function(index, item) {
                html += '<article class="item" data-item-id="' +
                    item.pgal_id + '">' +
                  '<figure>' +
                    '<img src="/uploads/galleryimages/' + item.pgal_id +
                    '/lt_' + item.first_image[0]['pimg_name'] + '" alt="" \/>' +
                  '</figure>' +
                  '<section class="content">' +
                      '<div class="details">' +
                          '<div class="details-inner">' +
                              '<p class="name">' + item._title_prefix + '<br>' +
                               item.lgal_title + '<\/p>' +
                              (item.pgal_staring_price ? '<p class="price">' +
                                item.pgal_starting_price + ' EUR<\/p>' : '') +
                            '<\/div>' +
                        '<\/div>' +
                    '<\/section>' +
                '<\/article>';
            });

            $masonryContainer.html( html );
            $masonryContainer.masonry( 'reloadItems' );
            $masonryContainer.masonry( 'layout', html );
          },
          error: function (error) {
            return;
          }
        });
      });
  });

  function isNumber(event, element) {
    if (!event) { return; }

    var charCode = (event.which) ? event.which : event.keyCode;

    if (charCode !== 190 && charCode > 31 &&
        (charCode < 48 || charCode > 57) &&
        (charCode < 96 || charCode > 105) &&
        (charCode < 37 || charCode > 40) &&
        charCode !== 110 && charCode !== 8 && charCode !== 46 ) {

      return false;
    }
  }

  // set active menu item
  var sections = $('article'),
    links = $('nav.content-menu a');

  links.on('click', function(){
        links.removeClass('active');
    $(this).addClass('active');
  });

  $(window).scroll(function() {
      var currentPosition = $(this).scrollTop();

      sections.each(function() {
          var top = $(this).offset().top,
              bottom = top + $(this).height();

          if (currentPosition >= top && currentPosition <= bottom) {
            links.removeClass('active');
            $('#link_' + $(this).attr('id')).addClass('active');
          }
      });
  });
})(jQuery);
