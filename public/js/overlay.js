var Overlay = (function () {
  var compiled = _.template($('#tpl-gallery-overlay').html());

  function Overlay (data) {
    if (!('id' in data)) {
      return;
    }

    this._id = data.id;
    this._tpl = compiled(data);
  }


  //
  // PUBLIC
  //

  /**
   * Renders the template
   */
  Overlay.prototype.renderTemplateAfter = function ($el) {
    $el.after(this._tpl);

    this._container = $('div.overlay-container');
    this._totalPriceEl = this._container.find('span.total-price');

    this._setupSelectBoxes();
    this._bindEvents();
    this._updateTotalPrice();
  };

  /**
   * Destroy overlay element
   */
  Overlay.prototype.destroy = function () {
    this._container.remove();
  };


  //
  // PRIVATE
  //

  /**
   * Template string
   */
  Overlay.prototype._tpl = null;

  /**
   * Overlay element container
   */
  Overlay.prototype._container = null;

  /**
   * Total price element
   */
  Overlay.prototype._totalPriceEl = null;

  /**
   * Selectbox elements
   */
  Overlay.prototype._sBoxes = null;

  /**
   * Main image
   */
  Overlay.prototype._image = null;

  /**
   * Image thumbnails elements
   */
  Overlay.prototype._thumbs = null;

  /**
   * Total price
   */
  Overlay.prototype._totalPrice = 0;

  /**
   * Updates total price
   */
  Overlay.prototype._updateTotalPrice = function () {
    var price = 0;
    _.forEach(this._sBoxes, function (sBox) {
      price += parseFloat(sBox.sBoxPrice);
    });
    this._totalPrice = price;
    this._totalPriceEl.html(this._totalPrice.toFixed(2).toString());
  };

  /**
   * Set up select boxes
   */
  Overlay.prototype._setupSelectBoxes = function () {
    this._sBoxes = this._container.find('div.selectbox');

    for (var i = 0; i < this._sBoxes.length; i++) {
      this._sBoxes[i].sBoxEl    = $(this._sBoxes[i])
                                    .find('a.selectbox-element');

      this._sBoxes[i].sBoxList  = $(this._sBoxes[i])
                                    .find('div.selectbox-list');

      var $firstItem = this._sBoxes[i].sBoxList
                        .find('a.selectbox-list-item')
                        .first();
      this._sBoxes[i].sBoxSize = $firstItem.attr('data-size');
      this._sBoxes[i].sBoxPrice = parseFloat($firstItem.attr('data-price'));
      this._sBoxes[i].sBoxId = $firstItem.attr('data-id');
    }
  };

  /**
   * Binds all events
   */
  Overlay.prototype._bindEvents = function () {
    this._bindSelectboxEvents();
    this._bindThumbnailEvents();
    this._bindCloseEvents();
    this._bindAddToCartEvents();
  };

  /**
   * Binds selectbox events
   */
  Overlay.prototype._bindSelectboxEvents = function () {
    var self = this;

    this._sBoxes.on('click', function (e) {
      if (e.target.className === "selectbox-element" ||
          e.target.parentNode.className === "selectbox-element") {

        this.sBoxList.toggle();
      } else if (e.target.className === "selectbox-list-item" ||
          e.target.parentNode.className === "selectbox-list-item") {

        var $item = e.target.className === "selectbox-list-item" ?
                      $(e.target) :
                      $(e.target.parentNode);

        this.sBoxEl.html($item.html());
        this.sBoxId = $item.attr('data-id');
        this.sBoxSize = $item.attr('data-size');
        this.sBoxPrice = parseFloat($item.attr('data-price'));
        self._updateTotalPrice();
        this.sBoxList.hide();
      }

      return false;
    });
  };

  /**
   * Binds thumbnail events
   */
  Overlay.prototype._bindThumbnailEvents = function () {
    this._thumbs = this._container.find('div.overlay-thumbnails');
    this._image = this._container.find('figure.overlay-image > img');

    var self = this;

    this._thumbs.on('click', 'figure.overlay-thumbnail', function () {
      var $this = $(this);
      if ($this.hasClass('selected')) { return; }

      self._image.attr('src', $this.attr('data-src'));
      self._thumbs.find('figure.selected').removeClass('selected');
      $this.addClass('selected');
    });
  };

  /**
   * Binds close button events
   */
  Overlay.prototype._bindCloseEvents = function () {
    var self = this;
    this._container.find('a.overlay-close').on('click', function (e) {
      e.preventDefault();
      self.destroy();
    });
  };

  /**
   * Binds add to cart button events
   */
  Overlay.prototype._bindAddToCartEvents = function () {
    var self = this;

    this._container.find('a.add-to-basket').on('click', function (e) {
      e.preventDefault();

        $.ajax({
          url: '/ajax/addgalleryitemtocart',
          type: 'GET',
          data: {
          	id: self._id,
          	size1: self._sBoxes[0].sBoxId,
          	size2: self._sBoxes[1].sBoxId,
          	size3: self._sBoxes[2].sBoxId
          },
          success: function (data) {
            var dataObj = JSON.parse(data);
	        $('#cart_qty').html(dataObj._qty);
	        self.destroy();
          },
          error: function (error) {
            return;
          }
        });
    });
  };

  return Overlay;
})();