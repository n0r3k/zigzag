$(function() {
	var typeList = $('.sortable'),
		orderItem = typeList.attr('id'),
		attribList = $('#subtype_list').find('ul'),
		/* marker form */
		ajaxUpload = $('#ajaxMarkerUpload'),
		fileList = ajaxUpload.find('#fileList'),
		imageList = ajaxUpload.find('#imageList'),
		formFields = ajaxUpload.find('input, button'),
		progress = ajaxUpload.find('.progress'),
		bar = progress.find('.bar'),
		percent = progress.find('.percent'),
		referenceID = $('#referenceID').val();

	/* Sorting */
	typeList.sortable({
		placeholder: 'sortable-placeholder',
		opacity: 0.7,
		update: function() {
			var serialized = typeList.sortable('serialize', { key: 'cls[]' });

			$.ajax({
				url: '/admin/ajax/order',
				type: 'POST',
				data: serialized + '&order_item=' + orderItem,
				success: function() {
					// console.log('ordering successful');
				},
				error: function() {
					alert("Something went wrong while sorting stable types!\nPlease try again...");
				}
			});
		}
	});
	typeList.disableSelection();

	attribList.sortable({
			placeholder: 'sortable-placeholder',
			opacity: 0.7,
			items: 'li:not(.no-sort)',
			update: function() {
				var serialized = attribList.sortable('serialize', { key: 'attribs[]' });

				$.ajax({
					url: '/admin/ajax/order',
					type: 'POST',
					data: serialized + '&order_item=stable_type_attrib',
					success: function() {
						 console.log('ordering successful');
					},
					error: function() {
						 console.log('error while ordering');
					}
				});
			}
	});
	attribList.disableSelection();
	
	/* Map Marker upload */
	/* Trigger file selection from custom button */
	ajaxUpload.find('#addFilesButton').click(function() {
		ajaxUpload.find('#filesToUpload').trigger('click');
	});
	
	ajaxUpload.ajaxForm({
		data: { ref_id: referenceID, ref_type: 'marker' },
	    beforeSend: function() {
	        fileList.empty();
		    formFields.attr('disabled', 'disabled');
		    progress.show();
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		success: function(responseText) {
			var responseObject = $.parseJSON(responseText);
			formFields.removeAttr('disabled', 'disabled');
			progress.hide();
	
			if(Object.keys(responseObject.data).length > 0) {
				$.each(responseObject.data, function(i, val) {
					imageList.append('<li class="image" id="img-' + val.id + '"><img src="' + val.path + val.name + '" alt="' + val.name + '" /><br/><a href="#" onclick="deleteImage(\'' + val.name + '\', ' + val.id + '); return false;" class="delete">delete</a></li>');
				});
			}
	
			if(Object.keys(responseObject.errors).length > 0) {
				$.each(responseObject.errors, function(i, val) {
					$.each(val, function(vi, vval) {
						imageList.prepend('<div class="image"><span class="error">' + vval + '</span></div>');
					});
				});
			}
		},
		error: function() {
			formFields.removeAttr('disabled', 'disabled');
			alert("Something went wrong with uploading files!\nPlease try again...");
		}
	});
});

function saveEntry(id) {
	var value = $('#entry_name_' + id).val();
	var classifier_code = $('#classifier-code').val();

	$.ajax({
		url: '/admin/ajax/saveEntry',
		type: 'POST',
		data: { code : classifier_code, id: id, value: value },
		success: function() {
			$('#entry\\[' + id + '\\]').html(
				'<td>' + value + '</td>'
				+ '<td>'
					+ '<a class="icn icn_edit" href="javascript:void(0);" onclick="editEntry(\'' + id + '\', \'' + value + '\');"></a>'
					+ '<a class="icn icn_trash" href="javascript:void(0);" onclick="if(confirm(\'Are you sure you want to permanently delete selected item?\')) { deleteEntry(\'' + id + '\'); } else { return false; }"></a>'
				+ '</td>'
			);
		},
		error: function() {
			alert("Something went wrong while saving " + type + "! Please try again...");
		}
	});
}

function editEntry(id, value) {
	$('#entry\\[' + id + '\\]').html(
		'<td style="background-color:#DDE4EA;"><input type="text" value="' + value + '" id="entry_name_' + id + '" class="editable" onkeypress="if(event.keyCode == 13) { saveEntry(\'' + id + '\'); }" /></td>'
		+ '<td style="background-color:#DDE4EA;">'
			+ '<a class="icn icn_edit" href="javascript:void(0);" onclick="saveEntry(\'' + id + '\');"></a>'
			+ '<a class="icn icn_trash" href="javascript:void(0);" onclick="if(confirm(\'Are you sure you want to permanently delete selected item?\')) { deleteEntry(\'' + id + '\'); } else { return false; }"></a>'
		+ '</td>'
	);
	
	$('#entry_name_' + id).focus().val($('#entry_name_' + id).val());
}

function addEntry()
{
	var classifier 		= $('tbody#classifier-content');
	var ts 				= new Date().getTime();

	classifier.prepend(
		'<tr id="entry[' + ts + ']">'
		+ '<td style="background-color:#DDE4EA;"><input type="text" id="entry_name[' + ts + ']" class="editable" onkeypress="if(event.keyCode == 13) { saveNewEntry($(this).val(), \'' + ts + '\'); }" /></td>'
		+ '<td style="background-color:#DDE4EA;">'
			+ '<a class="icn icn_edit" href="javascript:void(0);" onclick="saveNewEntry($(this).parent().parent().find(\'td:first\').find(\'input\').val(), \'' + ts + '\');"></a>'
			+ '<a class="icn icn_trash" href="javascript:void(0);" onclick="$(this).parent().parent().remove();"></a>'
		+ '</td>'
		+ '</tr>'
	);
	
	classifier.find('tr:first').find('td:first').find('input').focus();
}

function saveNewEntry(value, ts)
{
	if (!value) 
	{
		alert('What, no value?');
		return false;
	}

	var classifier_code = $('#classifier-code').val();

	$.ajax({
		url: '/admin/ajax/saveEntry',
		type: 'POST',
		data: { code : classifier_code, value: value },
		success: function(id) {
			if (!id)
			{
				alert('Error saving data!');
				return false;	
			}

			$('#entry\\[' + ts + '\\]').html(
				'<td>' + value + '</td>'
				+ '<td>'
					+ '<a class="icn icn_edit" href="javascript:void(0);" onclick="editEntry(\'' + id + '\', \'' + value + '\');"></a>'
					+ '<a class="icn icn_trash" href="javascript:void(0);" onclick="if(confirm(\'Are you sure you want to permanently delete selected item?\')) { deleteEntry(\'' + id + '\'); } else { return false; }"></a>'
				+ '</td>'
			).attr('id', 'entry[' + id + ']');
		},
		error: function() {
			alert("Something went wrong while saving " + type + "! Please try again...");
		}
	});
}

function deleteEntry(id)
{
	if(!id)
	{
		return false;
	}

	var classifier_code = $('#classifier-code').val();
	
	$.ajax({
		url: '/admin/ajax/deleteEntry',
		type: 'POST',
		data: { code : classifier_code, id: id },
		success: function() {
			$('#entry\\[' + id + '\\]')
				.fadeOut()
				.remove();
		},
		error: function() {
			alert("Something went wrong while deleting " + classifier_code + "! Please try again...");
		}
	});
}