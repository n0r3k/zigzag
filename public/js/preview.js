/* global $, document, console */

var Preview = (function () {
  var borderColor = '1px solid #000';

  //
  // CONSTRUCTOR
  //
  //
  function Preview (data) {
    $.extend(this._opts, data);
  }


  //
  // PUBLIC
  //

  /**
   * Generates preview layout.
   *
   * @return {object} layout.
   */
  Preview.prototype.generateTableLayout = function () {
    var self = this;
    var rows, cols, tab, cellSize, cellDimension;

    cols = Math.round(this._opts.width / 5);
    rows = Math.round(this._opts.height / 5);

    cellDimension = Math.floor(self._opts.containerHeight / rows);
    cellSize = cellDimension + 'px';

    // Table
    tab = document.createElement('table');
    tab.style.borderCollapse = 'collapse';
    tab.cellPadding = "0";
    tab.cellSpacing = "0";
    tab.style.marginLeft = '-' + (this._opts.containerWidth / 2) + 'px';
    tab.style.width = this._opts.containerWidth + 'px';
    tab.style.height = this._opts.containerHeight + 'px';

    for (var i = 0; i < rows; i++) {
      var row = tab.insertRow(i);

      for (var j = 0; j < cols; j++) {
        var cell = row.insertCell(j);

        cell.setAttribute('x', i);
        cell.setAttribute('y', j);
        cell.style.width = cellSize;
        cell.style.height = cellSize;
        cell.style.backgroundColor = self._opts.colors[i * cols + j];
      }
    }

    return tab;
  };

  /**
   * Set background color to an element.
   *
   * @param {object} element
   */
  Preview.prototype.setBackgroundColor = function ($el) {
    $el.css('background-color', this._opts.bottomColor);
  };


  //
  // PRIVATE
  //

  /**
   * Preview item's options.
   *
   * width - item width
   * height - item height
   * containerWidth - item container width
   * containerHeight - item container height
   * colors - preview cell colors
   */
  Preview.prototype._opts = {
    width: null,
    height: null,
    containerWidth: null,
    containerHeight: null,
    colors: null
  };

  return Preview;
})();
