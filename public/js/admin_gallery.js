$(function() {
	// Define vars
	var referenceID = $('#referenceID').val(),
		stateToggle = $('.state_toggle');
		
	/* Toggle stable state */
	stateToggle.click(function() { toggleState('gallery', stateToggle, referenceID, $(this).val()); });
});