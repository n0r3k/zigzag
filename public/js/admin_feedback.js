$(function() {
	// Define vars
	var referenceID = $('#referenceID').val(),
		stateToggle = $('.state_toggle');
		
	/* Toggle stable state */
	stateToggle.click(function() { toggleState('feedback', stateToggle, referenceID, $(this).val()); });
});