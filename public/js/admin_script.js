$(function() {
	//Tablesorter
	$('.tablesorter').tablesorter(); 
	
	// Changes gallery state
	$('.change_state').on('click', function() {
		var $this = $(this),
			ref_id = $this.val(),
			state;
		
		if($this.is(':checked')) state = 1; else state = 0;
		toggleState('gallery', $this, ref_id, state);
	});
	
	// Initialize redactor content
	$('.redactor_content').redactor({
    	path: '',
    	css: 'redactor_style.css'
    });
    
    initTabs();
    
    /* Image upload, delete & sorting (adverts & stables) */
    var ajaxUpload = $('#ajaxUpload'),
		imageList = ajaxUpload.find('#imageList'),
		fileList = ajaxUpload.find('#fileList'),
		formFields = ajaxUpload.find('input, button'),
		progress = ajaxUpload.find('.progress'),
		bar = progress.find('.bar'),
		percent = progress.find('.percent'),
		referenceID = $('#referenceID').val(),
		referenceType = $('#referenceType').val();
		
	/* Trigger file selection from custom button */
	ajaxUpload.find('#addFilesButton').click(function() {
		ajaxUpload.find('#filesToUpload').trigger('click');
	});
	
	/* Ajax form */
	ajaxUpload.ajaxForm({
		data: { ref_id: referenceID, ref_type: referenceType },
	    beforeSend: function() {
	        fileList.empty();
		    formFields.attr('disabled', 'disabled');
		    progress.show();
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		success: function(responseText) {
			var responseObject = $.parseJSON(responseText);
			formFields.removeAttr('disabled', 'disabled');
			progress.hide();
			
			if(Object.keys(responseObject.data).length > 0) {
				$.each(responseObject.data, function(i, val) {
					var input_label = '';
					
					if (referenceType == 'page') input_label = '<input type="text" value="' + val.path + 'f_' + val.name + '" onclick="this.setSelectionRange(0, this.value.length)">';

					imageList.append(
						'<li class="image" id="img-' + val.id + '">' +
						'<img src="' + val.path + 'st_' + val.name + '" alt="' + val.name + '" /><br/>' +
						input_label +
						'<a href="#" onclick="deleteImage(\'' + val.name + '\', ' + val.id + '); return false;" class="delete">Dzēst</a></li>');
				});
			}

			if(Object.keys(responseObject.errors).length > 0) {
				$.each(responseObject.errors, function(i, val) {
					$.each(val, function(vi, vval) {
						imageList.prepend('<div class="image"><span class="error">' + vval + '</span></div>');
					});
				});
			}
		},  
		error: function() {
			formFields.removeAttr('disabled', 'disabled');
			alert("Something went wrong with uploading files!\nPlease try again...");
		}
	});
	
	/* Sorting */
	imageList.sortable({
		placeholder: 'sortable-placeholder',
		opacity: 0.7,
		update: function() {
			var serialized = imageList.sortable('serialize', { key: 'images[]' });

			$.ajax({
				url: '/admin/ajax/order',
				type: 'POST',
				data: serialized + '&order_item=image',
				success: function() {
					// console.log('ordering successful');
				},
				error: function() {
					alert("Something went wrong while sorting images!\nPlease try again...");
				}
			});
		}
	}); 
	imageList.disableSelection();
});

function deleteImage(image_name, image_id) {
	var referenceID = $('#referenceID').val();

	$.ajax({
		url: '/admin/ajax/delete',
		type: 'POST',
		data: { 
			ref_id: referenceID,
			img_id: image_id,
			delete_item: 'image'
		},
		success: function() {
			$('#img-' + image_id).remove();
		},
		error: function() {
			alert("Something went wrong while deleteing image!\nPlease try again...");
		}
	});
}

function initTabs() {
	//Tabs & tab containers
	var tabContent = $('.tab_content'),
		tabTitles = $('ul.tabs li'),
		firstActiveTab = $('ul.tabs li:not(.hidden)').first(),
		firstActiveTabIndex = firstActiveTab.index();
		
	tabContent.css('display', 'none'); //Hide all content
	firstActiveTab.addClass('active'); //Activate first visible tab
	if(tabTitles.length == 0 || firstActiveTabIndex >= 0) {
		tabContent.eq(firstActiveTabIndex).css('display', 'block'); //Show first active & visible tab content
	}
	
	// If hash is used in URI
	if(window.location.hash) {
		var hash = window.location.hash;
			$attributes = $('#attributes'),
			$tabButtonContainer = $attributes.find('ul.tabs');
		
		setTimeout(function() {
			$(window).scrollTop(0);
		}, 0);
					
		if($tabButtonContainer.find(hash).length > 0) {
			var $tabButtons = $tabButtonContainer.find('li'),
				$tabContainer = $attributes.find('.tab_container'),
				$tabs = $tabContainer.find('.tab_content');
		
			$tabButtons.removeClass('active');	
			$tabButtonContainer.find(hash).addClass('active');
			$tabs.hide();
			$tabContainer.find($tabButtonContainer.find('.active>a').attr('href')).show();
		}
	}

	//On tab click event
	tabTitles.on('click', function() {
		var $this = $(this);
		tabTitles.removeClass('active'); //Remove any "active" class
		$this.addClass('active'); //Add "active" class to selected tab
		tabContent.css('display', 'none'); //Hide all tab content

		var activeTab = $this.find('a').attr('href'); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
	
	// Additional click event for attributes tabs
	$('#attributes').find('ul.tabs li').on('click', function() {
		var prevScrollTop = $(window).scrollTop();
		window.location.hash = $(this).attr('id');
		$(window).scrollTop(prevScrollTop);
		return false;
	});
}

function toggleState(type, el, ref_id, state) {
	$.ajax({
		url: '/admin/ajax/changestate',
		type: 'POST',
		data: { state: state, ref_id: ref_id, type: type },
		beforeSend: function() {
			el.attr('disabled', 'disabled');
		},
		success: function() {
			el.removeAttr('disabled');
		},
		error: function() {
			el.removeAttr('disabled');
			alert("Something went wrong while changing the state of stable!\nPlease try again...");
		}
	});
}

/* Creates a list of uploadable files */
function makeUploadFilesList() {
	var input = document.getElementById("filesToUpload"),
		ul = document.getElementById("fileList");
	while (ul.hasChildNodes()) {
		ul.removeChild(ul.firstChild);
	}
	for (var i = 0; i < input.files.length; i++) {
		var li = document.createElement("li");
		li.innerHTML = input.files[i].name;
		ul.appendChild(li);
	}
	if(!ul.hasChildNodes()) {
		var li = document.createElement("li");
		li.innerHTML = 'No Files Selected';
		ul.appendChild(li);
	}
}