/* global module */

module.exports = function (grunt) {
  grunt.registerTask('build', [
    'clean:build',
    'copy:build',
    'sass:build'
  ]);

  grunt.registerTask('workflow:build', [
    'build',
    'exec:copy_build',
    'watch:build'
  ]);

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-exec');

  grunt.initConfig({
    pkg: 'package.json',
    app: {
      source_dir: 'src',
      build_dir: 'build',
      dist_dir: 'dist'
    },

    clean: {
      build : '<%= app.build_dir %>',
      dist : '<%= app.dist_dir %>/src'
    },

    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: '<%= app.source_dir %>',
            src: ['**', '!sass/**'],
            dest: '<%= app.build_dir %>',
	        rename: function(dest, src) {
	          return dest + '/' + src.replace(/_gitignore$/, ".gitignore");
	        }
	      }
        ]
      }
    },

    sass: {
      build: {
        files: [
          {
            expand: true,
            cwd: '<%= app.source_dir %>/sass',
            src: ['main.scss'],
            dest: '<%= app.build_dir %>/css',
            ext: '.css'
          }
        ],
        // options: {
        //   loadPath: ["<%= app.source_dir %>/modules"]
        // }
      }
    },

    exec: {
      copy_build: {
        command: 'cp -r build/* ../public'
      }
    },

    watch: {
      build: {
        files: ['<%= app.source_dir %>/**/*'],
        tasks: ['build', 'exec:copy_build'],
        options: {
          livereload: true
        }
      }
    }
  });
};