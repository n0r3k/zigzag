var Cache = (function () {
  var instance, cache;

  function init() {
    cache = {};

    return {
      get: function (key) {
        return key in cache ? cache[key] : null;
      },
      set: function (key, data) {
        cache[key] = data;
      }
    };
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = init();
      }

      return instance;
    }
  };
})();
