/* global _, $, document, console, confirm, Preview, setTimeout */

var Constructor = (function () {
  var compiled = _.template($('#tpl-constructor').html());

  var active_color;
  var activeColorEl;
  var activeColorPattern;
  var mouse_up_coords;
  var mouse_down_coords;

  this.layout = [];
  this.bottomColor = null;

  // sizes
  var width;
  var height;

  // prices
  this.base_price = null;
  this.design_price = null;
  var total_price;

  this.type = null;
  this.sizeId = null;
  this.data = null;

  // Apstrade / aizdare
  this.aaId = null;
  
  //
  // CONSTRUCTOR
  //
  function Constructor (data) {
//    this.width = data.sizes[0].clss_width;
//    this.height = data.sizes[0].clss_height;

    this.base_price = data.sizes[0].clss_base_price;
    this.design_price = 0.00;

    this.data = data;
    this.type = data.type;
    this._tpl = compiled(data);
  }


  //
  // PUBLIC
  //

  Constructor.prototype.renderTemplateAfter = function ($el) {
    $el.after(this._tpl);

    this._container = $('div.constructor-container');
    this._setupSelectBoxes();
    this._loadLayoutFromSession(this.type);

    if ( typeof this.layout === 'undefined' )
    {
      this._setupConstructor( this.width, this.height );
    }

    this._bindEvents();
  };

  Constructor.prototype.destroy = function () {
    this._container.remove();
  };


  //
  // PRIVATE
  //

  Constructor.prototype._sBoxes = null;

  Constructor.prototype._bindEvents = function () {
    this._bindColorEvents();
    this._bindCloseEvents();
    this._bindOrderEvents();
    this._bindHoverEvents();
    this._bindSelectboxEvents();
  };

  Constructor.prototype._bindColorEvents = function () {
    var self = this,
        container = this._container.find('div.color-selector-content'),
        colors = container.find('span.color'),
        colorNext = container.find('a.color-next'),
        colorPrev = container.find('a.color-previous'),
        dropEl = this._container.find('div.droppable-target');

    activeColorEl = $(colors[0]);
    activeColorPattern = container.find('img');
    this._setActiveColor(activeColorEl);

    container.on('click', function (e) {
      e.stopPropagation();
      e.preventDefault();

      var $t = $(e.target);

      if ($t.hasClass('color-next')) {
        if (activeColorEl.index() === colors.length - 1) {
          self._setActiveColor(colors.first());
        } else {
          self._setActiveColor(activeColorEl.next());
        }
      } else if ($t.hasClass('color-previous')) {
        if (activeColorEl.index() === 0) {
          self._setActiveColor(colors.last());
        } else {
          self._setActiveColor(activeColorEl.prev());
        }
      } else if ($t.hasClass('color')) {
        self._setActiveColor($t);
      }
    });

    if (dropEl.length) {
      colors.on('dragstart', function (e) {
        e.originalEvent.dataTransfer.effectAllowed = 'copy';
        e.originalEvent
          .dataTransfer
          .setData('color', $(this).attr('data-color-id'));
      });

      dropEl.on('click', function () {
		dropEl.css('background-color', active_color);
        self.bottomColor = active_color;
        self._saveLayoutToSession();
      });

      dropEl.on('dragenter', function () {
        dropEl.addClass('drop');
        return false;
      });

      dropEl.on('dragleave', function () {
        dropEl.removeClass('drop');
      });

      dropEl.on('dragover', function (e) {
        if (e.preventDefault) { e.preventDefault(); }
        dropEl.addClass('drop');
        e.originalEvent.dataTransfer.dropEffect = 'copy';
        return false;
      });

      dropEl.on('drop', function (e) {
        if (e.stopPropagation) { e.stopPropagation(); }
        var color = e.originalEvent.dataTransfer.getData('color');
        dropEl.css('background-color', '#' + color).removeClass('drop');
        self.bottomColor = '#' + color;
        self._saveLayoutToSession();
        return false;
      });
    }
  };

  Constructor.prototype._setActiveColor = function (el) {
    var cId = el.attr('data-color-id');
    active_color = '#' + cId;
    activeColorPattern.attr(
      'src',
      '/images/darbnica/constructor-colors/color-' + cId + '.jpg'
    );
    activeColorEl.removeClass('active');
    el.addClass('active');
    activeColorEl = el;
  };

  Constructor.prototype._loadPreviewsFromSession = function () {
      var $factory = $('div.factory-container div.factory'),
          $pricingContainer = $('div.buy div.pricing');

      $.ajax({
        url: '/ajax/loadpreviewsfromsession',
        type: 'POST',
        data: { types: [
            'spilvendrana-1', 'spilvendrana-2', 'palags', 'virspalags'
          ]
        },
        success: function (data) {
          var dataObj = JSON.parse(data),
              previewData = {},
              preview,
              layout;

          var $containers = {
            'virspalags': $factory.find('div.trigger.virspalags'),
            'palags': $factory.find('div.trigger.palags'),
            'spilvendrana-1': $factory.find('div.trigger.spilvendrana-1'),
            'spilvendrana-2': $factory.find('div.trigger.spilvendrana-2')
          };

          var $prices = {
            'virspalags': $pricingContainer.find('span.price-virspalags'),
            'palags': $pricingContainer.find('span.price-palags'),
            'spilvendrana': $pricingContainer.find('span.price-spilvendrana'),
            'total': $pricingContainer.find('div.total-price span.price')
          };

          total_price = 0;
          var palags_price = 0;
          var virspalags_price = 0;
          var spilvendrana_price = 0;

          $.each(dataObj, function (i, item) {
            if (item) {
              if (i !== 'palags') {
                previewData = {
                  width: item.size.width,
                  height: item.size.height,
                  colors: item.layout
                };

                previewData.containerWidth = $containers[i].width();
                previewData.containerHeight = $containers[i].height();

                preview = new Preview(previewData);
                $containers[i]
                  .find('table').remove().end()
                  .prepend(preview.generateTableLayout())
                  .addClass('has-preview');

                if (i === 'virspalags') {
                  $containers[i].height($containers[i].find('table').height());
                }

                // prices
                var tmp_price =
                  Number(item.size.base_price) + Number(item.design_price);

                if (i === 'virspalags') {
                  virspalags_price += tmp_price;
                } else {
                  spilvendrana_price += tmp_price;
                }

                total_price += tmp_price;

                // </- prices
              } else {
                previewData = {
                  bottomColor: item.bottom_color
                };

                preview = new Preview(previewData);
                preview.setBackgroundColor($containers[i]);

                palags_price += Number(item.size.base_price);
                total_price += palags_price;
              }
            } else {
              $containers[i].removeClass('has-preview');
            }
          });

          $prices['spilvendrana'].html(spilvendrana_price.toFixed(2)+' &euro;');
          $prices['virspalags'].html(virspalags_price.toFixed(2) + ' &euro;');
          $prices['palags'].html(palags_price.toFixed(2) + ' &euro;');
          $prices['total'].html(total_price.toFixed(2) + ' &euro;');
        },
        error: function (err) {
          // console.log(err);
        }
      });
  };

  Constructor.prototype._bindCloseEvents = function () {
    var self = this;
    this._container.find('a.close').on('click', function (e) {
      e.preventDefault();
      self.destroy();

      self._loadPreviewsFromSession();
    });
  };

  Constructor.prototype._bindHoverEvents = function () {
    $('div.size').hover(
   	 	function(){
   	 		$('p.size').fadeIn();
   	 	},
   	 	function(){
   	 		$('p.size').hide();
   	 	}
    );

    $('div.color-selector').hover(
   	 	function(){
   	 		$('p.colors').fadeIn();
   	 	},
   	 	function(){
   	 		$('p.colors').hide();
   	 	}
    );

    $('div.aa').hover(
   	 	function(){
   	 		$('p.options').fadeIn();
   	 	},
   	 	function(){
   	 		$('p.options').hide();
   	 	}
    );

    $('div.drop-bottom-color').hover(
   	 	function(){
   	 		$('p.bottom-color').fadeIn();
   	 	},
   	 	function(){
   	 		$('p.bottom-color').hide();
   	 	}
    );
  };

  Constructor.prototype._bindOrderEvents = function () {
    var self = this;

    this._container.find('a.order').on('click', function (e) {
      e.preventDefault();

      var empties = [];

      var $droppable = self._container.find('div.droppable-target');

      if($droppable.css('background-color') === 'rgba(0, 0, 0, 0)') {
        empties.push($droppable[0]);
      }

      $('#design-table tr').each(function() {
        $(this).find('td').each(function() {
          if ($(this).css('background-color') === 'rgba(0, 0, 0, 0)') {
            empties.push(this);
          }
        });
      });

      var blink = function (i) {
        if (i === 0) {
          return;
        }

        $(empties).css('background-color', '#c2cc00');
        setTimeout(function () {
          $(empties).css('background-color', 'rgba(0, 0, 0, 0)');
          setTimeout(function () {
            blink(i-1);
          }, 200);
        }, 200);
      };

      if (empties.length) {
        blink(3);
      } else {
        self.destroy();
        self._loadPreviewsFromSession();
      }
    });
  };

  Constructor.prototype._bindSelectboxEvents = function () {
    var self = this;

    this._sBoxes.on('click', function (e) {
      if (e.target.className === "selectbox-element" ||
          e.target.parentNode.className === "selectbox-element") {

        this.sBoxList.toggle();

      } else if (e.target.className === "selectbox-list-item" ||
          e.target.parentNode.className === "selectbox-list-item") {


          var $item = e.target.className === "selectbox-list-item" ?
                        $(e.target) :
                        $(e.target.parentNode);

          this.sBoxEl.html($item.html());
          this.sBoxSize = $item.attr('data-size');
          this.sBoxList.hide();

          if (this.sBoxList.hasClass('sizes') &&
              confirm(self.data.confirm_message)) {

            self.layout = null;
            self.base_price = $item.attr('data-base-price');
            self._setPrices();
            self._setupConstructor(
              $item.attr('data-width'),
              $item.attr('data-height')
            );
            self.sizeId = $item.attr('data-size-id');
            self._saveLayoutToSession();
          } else if (this.sBoxList.hasClass('apstrade-aizdare')) {
            self.aaId = $item.attr('data-aa-id');
            self._saveLayoutToSession();
          }

        } else {
          this.sBoxList.hide();
        }

      return false;
    });
  };

  Constructor.prototype._setupSelectBoxes = function () {
    this._sBoxes = this._container.find('div.selectbox');

    for (var i = 0; i < this._sBoxes.length; i++) {
      this._sBoxes[i].sBoxEl = $(this._sBoxes[i]).find('a.selectbox-element');
      this._sBoxes[i].sBoxList = $(this._sBoxes[i]).find('div.selectbox-list');
    }
  };

  Constructor.prototype._saveLayout = function () {
    var self = this;
    self.layout = [];
    $('#design-table tr').each(function(){
        $(this).find('td').each(function() {
            self.layout.push($(this).css('background-color'));
        });
    });

    self.bottomColor = $('div.droppable-target').css('background-color');
  };

  Constructor.prototype._loadLayout = function () {
    if ( typeof this.layout === 'undefined' ) {
      return false;
    }

    var self = this;
    var i = 0;

    $('#design-table tr').each(function() {
      $(this).find('td').each(function() {
        $(this).css('background-color', self.layout[i]);
        i++;
      });
    });

    $('div.droppable-target').css('background-color', self.bottomColor);
  };

  Constructor.prototype._calcPrice = function () {
    var seam_cnt = 0;

    $('#design-table tr').each(function() {
      $(this).find('td').each(function() {
        if ($(this).next().attr('x') !== undefined &&
            $(this).css('background-color') !==
            $(this).next().css('background-color')) {

          seam_cnt++;
        }

        var x_val = Number($(this).attr('x')) + 1;
        var y_val = Number($(this).attr('y'));

        var el = document.getElementById('x[' + x_val + ']y[' + y_val + ']');

        if (null !== el &&
            $(this).css('background-color') !== el.style.backgroundColor) {

          seam_cnt++;
        }
      });
    });

    this.design_price = seam_cnt * 5 * 0.02;

    this._setPrices();
  };

  Constructor.prototype._setPrices = function () {
    // base price
    this._container
      .find('div.costs')
      .find('p.base-price')
      .find('span.price')
      .html(Number(this.base_price).toFixed(2) + ' &euro;');

    // design price
    this._container
        .find('div.costs')
        .find('p.design')
        .find('span.price')
        .html(Number(this.design_price).toFixed(2) + ' &euro;');

    // price total
    var total_price = Number(this.design_price) + Number(this.base_price);

    this._container
        .find('div.total-price')
        .find('span.price')
        .html(total_price.toFixed(2) + ' &euro;');
  };

  Constructor.prototype._colorSquare = function (mouse_down_coords,
      mouse_up_coords) {

    if (!mouse_down_coords ||
        !mouse_up_coords ||
        mouse_down_coords === mouse_up_coords) {

      return false;
    }

    var x_begin, x_end, y_begin, y_end;

    if (mouse_down_coords[0] > mouse_up_coords[0]) {
      x_begin = mouse_up_coords[0];
      x_end = mouse_down_coords[0];
    } else {
      x_begin = mouse_down_coords[0];
      x_end = mouse_up_coords[0];
    }

    if (mouse_down_coords[1] > mouse_up_coords[1]) {
      y_begin = mouse_up_coords[1];
      y_end = mouse_down_coords[1];
    } else {
      y_begin = mouse_down_coords[1];
      y_end = mouse_up_coords[1];
    }

    for (var i = x_begin; i <= x_end; i++) {
      for(var j = y_begin; j <= y_end; j++) {
        var el = document.getElementById('x[' + i + ']y[' + j + ']');
        el.style.backgroundColor = active_color;
      }
    }
  };

  Constructor.prototype._setupConstructor = function (y, x) {
    if (this.type === 'palags') {
      return false;
    }

    if (!x || !y) {
      x = this.data['sizes'][0]['clss_width'];
      y = this.data['sizes'][0]['clss_height'];
    }

    var self = this,
        poz = this._container.find('div.builder'),
        tab = document.createElement('table'),
        tab_width = 375;

    x = Math.round(x / 5);
    y = Math.round(y / 5);

    poz.html(tab);

    tab.id = 'design-table';
    tab.style.borderCollapse = 'collapse';
    tab.style.width = tab_width + 'px';
    tab.style.height = tab_width + Math.floor((x - y) * (tab_width / y)) + 'px';

    for (var i = 0; i < x; i++) {
      var row = tab.insertRow(i);

      for(var j = 0; j < y; j++) {
        var cell = row.insertCell(j);

        cell.id = 'x[' + i + ']y[' + j + ']';

        cell.setAttribute('x', i);
        cell.setAttribute('y', j);

        cell.style.border = '1px solid #000';
        cell.style.width = Math.floor(tab_width / y) + 'px';
        cell.style.height = this.width;
        cell.style.backgroundColor = 'rgba(0, 0, 0, 0)';

        cell.onclick = function () {
          this.style.backgroundColor =
            this.style.backgroundColor === active_color ?
              'rgba(0, 0, 0, 0)' :
              active_color;
        };

        cell.onmousedown = function () {
          mouse_down_coords = [
            Number(this.getAttribute('x')),
            Number(this.getAttribute('y'))
          ];
          self._saveLayout();
        };

        cell.onmouseup = function () {
          mouse_up_coords = [
            Number(this.getAttribute('x')),
            Number(this.getAttribute('y'))
          ];
          self._saveLayout();
          self._saveLayoutToSession();
          mouse_down_coords = false;
          mouse_up_coords = false;
        };
      }
    }

    $('#design-table tr td').bind({
      mouseenter: function() {
        if (mouse_down_coords) {
          mouse_up_coords = [
            Number($(this).attr('x')),
            Number($(this).attr('y'))
          ];

          if (mouse_down_coords && mouse_up_coords) {
            if (self.layout)
            {
                if (self.layout)
                {
                  self._loadLayout();
                }

                self._colorSquare(mouse_down_coords, mouse_up_coords);
              }
          }
        }
      },
      mouseup: function() {
        self._calcPrice();
      },
      click: function() {
        self._calcPrice();
      }
    });
  };

  Constructor.prototype._saveLayoutToSession = function () {
    var self = this;

    if (typeof self.sizeId === 'undefined' || !self.sizeId) {
      self.sizeId = self._container
        .find('div.size.dropdown div.sizes a')
        .first().attr('data-size-id');
    }

    if (typeof self.aaId === 'undefined' || !self.aaId) {
      self.aaId = self._container
        .find('div.aa.dropdown div.apstrade-aizdare a')
        .first().attr('data-aa-id');
    }

    $.ajax({
      url: '/ajax/savelayouttosession',
      type: 'POST',
      data: {
        type: self.type,
        size: self.sizeId,
        aaId: self.aaId,
        layout: self.layout,
        bottom_color: self.bottomColor,
        base_price: self.base_price,
        design_price: self.design_price
      },
      success: function (data) {
        // console.log(data);
      }
    });
  };

  Constructor.prototype._loadLayoutFromSession = function () {
    var self = this;

    $.ajax({
      url: '/ajax/loadlayoutfromsession',
      type: 'POST',
      data: { type: self.type },
      success: function (data) {
        var dataObj = JSON.parse(data);

        if (dataObj)
        {
          self.layout = dataObj.layout;
          self.bottomColor = dataObj.bottom_color;
          self.base_price = dataObj.size['base_price'];

          if (typeof dataObj.size !== "undefined") {
            self.sizeId = dataObj.size.id;
            var $sizeDropdown = $('div.size.dropdown');

            $sizeDropdown.find('div.sizes a').each(function (i, item) {
              if ($(item).attr('data-size-id') === self.sizeId) {
                $sizeDropdown
                  .find('a.selectbox-element')
                  .html($(item).html());
              }
            });
          }

          if (typeof dataObj.aaId !== "undefined") {
            self.aaId = dataObj.aaId;
            var $aaDropdown = $('div.aa.dropdown');

            $aaDropdown.find('div.apstrade-aizdare a').each(function (i, item) {
              if ($(item).attr('data-aa-id') === self.aaId) {
                $aaDropdown
                  .find('a.selectbox-element')
                  .html($(item).html());
              }
            });
          }

          self.width = dataObj.size['width'];
          self.height = dataObj.size['height'];
        }

        self._setupConstructor(self.width, self.height);
        self._loadLayout();
        self._calcPrice();
        self._setPrices();
      }
    });
  };

  return Constructor;
})();
